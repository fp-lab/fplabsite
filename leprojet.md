---
layout: page
background: darker
title: Le projet
date: 2020-12-10
author:
categories:
tags:
lead_text:
custom-css-list:
---

## En bref

Le FabPart Lab explore et met en œuvre la fabrique de la participation du Labex [Les passés dans le présent](http://passes-present.eu).

Il a pour mission d’accompagner les projets du labex dans le développement de leur dimension contributive et participative et d'enrichir la réflexion autour de la médiation et la valorisation numérique des mémoires et des patrimoines.

Pour orienter les projets dans leurs dynamiques participatives et dans le cycle de vie de leurs données, nous construisons [une base de connaissances](/ressources.html) sur les différentes phases de la recherche et les outils et services concernés.

Le FabPart Lab est aussi un lieu de rencontre, sous la forme d'[ateliers pratiques et théoriques](/evenements.html).<br/>

_Le projet est développé dans le cadre du Labex Les passés dans le présent (réf. ANR-11-LABX-0026-01)_

## Les objectifs

Le projet FabPart Lab vise à construire un lieu physique et virtuel d'échange et d'expérimentation autour de la participation, de la médiation et de la valorisation numérique des mémoires et des patrimoines dans le cadre du Labex _Les passés dans le présent_.

Face à la place grandissante que ces enjeux occupent dans le domaine de la Culture, le FabPart Lab a vocation à&nbsp;:

1. **Fédérer les acteurs du Labex qui portent des projets et des plateformes numériques de participation, de médiation et/ou de valorisation** dans un réseau local unique avec un fonctionnement participatif. En effet, plusieurs acteurs du Labex ont développé ou sont intéressés par le développement de projets et de plateformes qui facilitent la participation, la médiation et la valorisation autour d’objets culturels, patrimoniaux ou mémoriels au centre de leurs recherches. Cependant, ces travaux sont aujourd’hui éparpillés, parfois restent à niveau de prototype ou ils ont du mal à trouver une forme de pérennisation. Le FabPart Lab propose un espace unique où conduire leur expérimentation.
   <!-- Ce réseau vise à être un espace d’échange et de partage d’expériences à niveau local qui réunit chercheurs, ingénieurs, professionnels et doctorants. L’équipe du FabPart Lab mettra en place une série de dispositifs participatifs (séminaires périodiques, wiki, workshops etc.) pour faciliter l’échange d’expériences et la partage des ressources et de bonnes pratiques entre les membres du réseau et le montage de projets collectifs. Les ingénieurs des différentes équipes joueront un rôle clé dans cette action de partage et d’échange de compétences.  -->
2. **Augmenter le potentiel d’innovation du Labex dans ce secteur à travers la construction d’un incubateur de nouveaux projets.** Au travers d'un [appel à projets continu](/actions/sprint.html), le FabPart choisira périodiquement des projets qui seront suivis pour la durée d’un mois. En fonctionnant comme incubateur, le FabPart Lab permettra au porteur de projet de vérifier la faisabilité de son projet, choisir les technologies nécessaires et trouver les bons partenaires.  
3. **Renforcer la réflexion autour de la médiation, participation et valorisation numériques des mémoires et des patrimoines dans le cadre interdisciplinaire proposé par le labex.** Cette réflexion mobilise notamment les sciences de la communication, la science politique, la sociologie politique et de la participation des citoyens dans les politiques publiques. De ce point de vue, les collaborations avec les institutions patrimoniales se révèlent cruciales, en particulier le co-portage du projet avec l’INA. Cette réflexion permettra la mise en œuvre d’approches innovantes dans les domaines de recherche, de nouvelles collaborations et des méthodes d’enseignement novatrices
4. **Renforcer la visibilité et la notoriété du labex aux niveaux national et international autour des enjeux de la médiation, participation et valorisation numérique des patrimoines et des mémoires** au travers de la construction d’un carnet de recherche qui restituera les avancements intermédiaires du projet
5. **Renforcer le lien entre formation et recherche dans ce domaine en facilitant l’accompagnement des projets pédagogiques** sous forme d’accompagnement de projets tuteurés ou d’interventions ponctuelles. Le FabPart Lab pourra également accueillir des stages de recherche
6. **Créer un écosystème robuste de collaboration entre institutions et chercheurs intéressés par la participation culturelle en facilitant la co-construction des projets.** A long terme, l’action du FabPart Lab vise à créer un vrai dialogue avec la société et le tissu urbain en accueillant des projets portés par des institutions ou associations. Cet écosystème ambitionne de faciliter la pérennisation des projets en mettant en relation des acteurs porteurs de projets et des acteurs de la pérennisation, notamment la TGIR-Humanum.

## Les partenaires

Au sein du Labex :

- [Laboratoire Dispositifs d'Information et de Communication à l'Ère Numérique](https://dicen-idf.org/) (DICEN-IDF) - EA 7339
- [Institut national de l'audiovisuel](https://ina.fr) (INA)
- [La contemporaine](http://www.lacontemporaine.fr/), bibliothèque, archives, musée des mondes contemporains
- [Musée du quai Branly](http://www.quaibranly.fr/)
- [Archives nationales](https://www.archives-nationales.culture.gouv.fr/)
- [Laboratoire d'Ethnologie et de Sociologie Comparative](https://lesc-cnrs.fr/fr) (LESC) - UMR 7186
- [Archéologies et Sciences de l'Antiquité](http://www.arscan.fr/) (ArScAn) - UMR 7041
- [Maison des Sciences de l'Homme Mondes](https://www.mshmondes.cnrs.fr/) - USR 3225

et les autres partenaires associés :

- [SOPHIAPOL](https://sophiapol.parisnanterre.fr/presentation/sophiapol-unite-de-recherche-en-sociologie-philosophie-et-anthropologie-politiques-63673.kjsp) - Unité de recherche en sociologie, philosophie et anthropologie politiques - EA 3932 
- [Centre de recherches sociologiques et politiques de Paris](http://www.cresppa.cnrs.fr/) (CRESPPA) - UMR 7217
- [Digital Humanities Lab of Yale](https://dhlab.yale.edu/)
- [TGIR Huma-Num](https://www.huma-num.fr/)
