---
layout: page
background: lighter
title: Journée d'étude et appel à contributions
subtitle: «&nbsp;Valorisations et appropriations numériques des patrimoines et des mémoires. Communautés et pratiques d’écritures&nbsp;»
date: 2021-11-23
author:
categories:
tags:
lead_text:
custom-css-list:
---

<style>
  h3 {
    margin: 1.7em 0 1em;
  }
  :target::before {
    content: "";
    display: block;
    height: 130px; /* fixed header height*/
    margin: -130px 0 0; /* negative fixed header height */
  }
</style>

<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#appel-à-contributions">Appel à contributions</a></li>
<li><a href="#modalités-de-soumission">Modalités de soumission</a></li>
<li><a href="#envoi-des-propositions">Envoi des propositions</a></li>
<li><a href="#calendrier">Calendrier</a></li>
<li><a href="#comité-dorganisation">Comité d’organisation</a></li>
<li><a href="#comité-scientifique">Comité scientifique</a></li>
</ul>
</nav>


### Appel à contributions

**Date limite de soumission : le 28 février 2022**

**Journée d'étude : le 14 avril 2022**

<div style="width:40%;float:right; margin-left:1em;">
<iframe src="/assets/pdf/Appel_Valorisation_et_Appropriation_Patrimoines_et_memoires_nov2021.pdf" width="100%" height="500px">
</iframe>
<p style="float:right; font-size: smaller;">
<a href="/assets/pdf/Appel_Valorisation_et_Appropriation_Patrimoines_et_memoires_nov2021.pdf">Ouvrir le pdf</a>
</p>
</div>

<p>Comment le num&eacute;rique transforme-t-il les pratiques de valorisation et d&rsquor;appropriation des m&eacute;moires et des patrimoines ? Comment la recherche accompagne-t-elle les pratiques m&eacute;morielles et patrimoniales de la soci&eacute;t&eacute; &agrave; travers la mise &agrave; disposition d&rsquor;outils num&eacute;riques ? Quelles m&eacute;moires et quels patrimoines permet-il de construire pour le futur ?</p>
<p>Cette journ&eacute;e d&rsquor;&eacute;tude explorera ces questions &agrave; partir des pratiques collectives d&rsquor;&eacute;criture num&eacute;rique g&eacute;n&eacute;r&eacute;es par les dynamiques d&rsquor;appropriation et de valorisation de collections patrimoniales et d&rsquor;objets m&eacute;moriels <span class="citation" data-cites="beaudouin_forme_2018">(Beaudouin <a href="#ref-beaudouin_forme_2018">2018</a>)</span>. Qu&rsquor;elles s&rsquor;inscrivent dans des dispositifs institutionnels de m&eacute;diation <span class="citation" data-cites="navarro_mediation_2019 appiotti__2021 juanals_strategies_2020">(Navarro et Renaud <a href="#ref-navarro_mediation_2019">2019</a> ; Appiotti et Sandri <a href="#ref-appiotti__2021">2021</a> ; Juanals et Minel <a href="#ref-juanals_strategies_2020">2020</a>)</span>, dans des exp&eacute;rimentations scientifiques <span class="citation" data-cites="mellet_penser_2020 chupin_documentarisation_2016 moirez_construire_2017 de_champs_testaments_2018 casemajor_pratiques_2018">(Mellet <a href="#ref-mellet_penser_2020">2020</a> ; Chupin <a href="#ref-chupin_documentarisation_2016">2016</a> ; Moirez <a href="#ref-moirez_construire_2017">2017</a> ; Champs et al. <a href="#ref-de_champs_testaments_2018">2018</a> ; Casemajor, Bellavance, et Sirois <a href="#ref-casemajor_pratiques_2018">2018</a>)</span> ou dans des initiatives citoyennes ou associatives de sauvegarde et de curation <span class="citation" data-cites="bideran_contribuer_2020 severo_imperatif_2021">(Bideran et Wenz <a href="#ref-bideran_contribuer_2020">2020</a> &NonBreakingSpace;; Severo <a href="#ref-severo_imperatif_2021">2021</a>)</span>, ces pratiques collectives font &eacute;merger de nouvelles formes de m&eacute;moires et de patrimoines.</p>
<p>Ces pratiques renvoient bien entendu aux probl&eacute;matiques li&eacute;es &agrave; la participation culturelle dans les sciences <span class="citation" data-cites="bernard_recherche_2019">(Bernard et al. <a href="#ref-bernard_recherche_2019">2019</a>)</span>, que l&rsquor;on peut envisager dans ses diff&eacute;rentes formes, de la collaboration &agrave; l&rsquor;annotation, du <em>crowdsourcing</em> &agrave; la cr&eacute;ation. &Agrave; partir de l&rsquor;exp&eacute;rience construite dans le cadre du projet FabPart Lab du Labex <em>Les pass&eacute;s dans le pr&eacute;sent</em>, la rencontre vise d&rsquor;un c&ocirc;t&eacute; &agrave; &eacute;largir le regard &agrave; des exp&eacute;riences externes au Labex et aux mondes acad&eacute;mique et institutionnel, et d&rsquor;un autre c&ocirc;t&eacute; &agrave; d&eacute;velopper une perspective originale &agrave; partir de la notion d&rsquor;&eacute;criture.</p>
<p>Nous nous int&eacute;resserons en effet aux &laquo;pratiques d&rsquor;&eacute;critures&raquo; induites par les dispositifs num&eacute;riques de valorisation et d&rsquor;appropriation patrimoniale et m&eacute;morielle. Nous comprenons ici le terme d&rsquor;&eacute;criture <span class="citation" data-cites="bachimont_ingenierie_2007 crozat_elements_2012 merzeau_intelligence_2013 candel_numerique_2019">(Bachimont <a href="#ref-bachimont_ingenierie_2007">2007</a> ; Crozat et al. <a href="#ref-crozat_elements_2012">2012</a> ; Merzeau <a href="#ref-merzeau_intelligence_2013">2013</a><a href="#ref-merzeau_intelligence_2013">a</a> ; Candel et al. <a href="#ref-candel_numerique_2019">2019</a>)</span> dans sa conception large, int&eacute;grant toutes les formes d&rsquor;inscriptions que les praticien&middot;ne&middot;s sont amen&eacute;&middot;e&middot;s &agrave; produire lors des processus de m&eacute;diation et d&rsquor;appropriation, que l&rsquor;on parle de leurs annotations (commentaires, notations, enrichissements), de leurs r&eacute;&eacute;critures (republication, redocumentarisation, mashup), de leurs conversations (micro-blogging, discussions), ou encore de leurs traces visibles ou non, intentionnelles ou non (donn&eacute;es de consultation, d&rsquor;utilisation), etc.</p>
<p>Par ces &eacute;critures, nous voulons porter l&rsquor;attention aux &laquo;communaut&eacute;s d&rsquor;&eacute;criture&raquo; <span class="citation" data-cites="sauret_revue_2020">(Sauret <a href="#ref-sauret_revue_2020">2020</a>)</span>. Caract&eacute;ris&eacute;es par une pratique performative d&rsquor;&eacute;criture et de r&eacute;&eacute;criture, ces communaut&eacute;s se construisent autour d&rsquor;une activit&eacute; partag&eacute;e de circulation et d&rsquor;appropriation des objets et des ressources <span class="citation" data-cites="merzeau_editorialisation_2013">(Merzeau <a href="#ref-merzeau_editorialisation_2013">2013</a><a href="#ref-merzeau_editorialisation_2013">b</a>)</span>. Elles g&eacute;n&egrave;rent, du m&ecirc;me coup, des ressources communes dont les modalit&eacute;s de propri&eacute;t&eacute; partag&eacute;e sont &agrave; questionner <span class="citation" data-cites="coriat_retour_2015">(Coriat <a href="#ref-coriat_retour_2015">2015</a>)</span>. Leurs observations ont pu montrer comment ces communaut&eacute;s de praticien&middot;ne&middot;s, y compris hors du champ acad&eacute;mique, &eacute;laborent des savoirs complexes, ancr&eacute;s dans les pratiques et les territoires et &agrave; m&ecirc;me de faire &eacute;merger et d&rsquor;organiser une intelligence collective autour des patrimoines et des m&eacute;moires. Cela est le cas du projet SAWA du <em>Labex Les pass&eacute;s dans le pr&eacute;sent</em> o&ugrave; la cr&eacute;ation d&rsquor;une plateforme num&eacute;rique de co-&eacute;criture a permis de construire une communaut&eacute; f&eacute;d&eacute;rant praticien&middot;ne&middot;s et chercher&middot;e&middot;s autour des savoirs autochtones wayana- apala&iuml;. Nous pouvons aussi citer les nombreuses initiatives d&rsquor;&eacute;criture num&eacute;rique autour de la Grande Guerre port&eacute;es principalement par des amateurs <span class="citation" data-cites="beaudouin_web_2018">(Beaudouin, Chevallier, et Maurel <a href="#ref-beaudouin_web_2018">2018</a>)</span>. Similairement, l&rsquor;exemple de l&rsquor;initiative GIF IT UP montre comment une forme d&rsquor;&eacute;criture particuli&egrave;re vient revisiter et mobiliser des collections patrimoniales (Europeana) pour en produire des interpr&eacute;tations nouvelles <span class="citation" data-cites="strzelichowska_europeana_2021">(Strzelichowska <a href="#ref-strzelichowska_europeana_2021">2021</a>)</span>.</p>
<p>Comment accueillir ces savoirs, quelle l&eacute;gitimit&eacute; ont-ils, et plus largement, quelles m&eacute;moires collectives g&eacute;n&egrave;rent-ils pour le futur ? Quel est l&rsquor;impact de cette ouverture &agrave; de nouveaux formats et &agrave; de nouveaux acteurs sur les processus de patrimonialisation ? L&agrave; o&ugrave; les institutions tendent &agrave; questionner la scientificit&eacute; de ces processus, il n&rsquor;est pas inutile de renverser la question, en se demandant par exemple comment des pratiques d&rsquor;&eacute;criture ancr&eacute;es et situ&eacute;es peuvent assurer une production de connaissances scientifiques collaboratives. De quelles dynamiques d&rsquor;autorit&eacute; et de l&eacute;gitimation sont-elles alors le fruit ? Y a-t-il dans ces pratiques des formes de validation susceptibles de garantir une certaine rigueur et scientificit&eacute; ? Peuvent-elles favoriser l&rsquor;adoption des politiques d&rsquor;ouverture et des communs dans l&rsquor;agenda des institutions culturelles ?</p>
<p>L&rsquor;objectif de cette journ&eacute;e sera alors de questionner ce type de pratiques d&rsquor;&eacute;criture, les communaut&eacute;s qui les alimentent et surtout de r&eacute;fl&eacute;chir aux caract&eacute;ristiques des objets patrimoniaux et m&eacute;morielles qu&rsquor;elles d&eacute;finissent ou red&eacute;finissent et qui sont destin&eacute;es &agrave; marquer le futur de notre soci&eacute;t&eacute;.</p>
<p>Les contributions attendues pourront s&rsquor;inscrire dans les axes th&eacute;matiques suivants :</p>
<ul>
<li><strong>Collectif et m&eacute;moire</strong> : dans quelles mesures ces pratiques d&rsquor;&eacute;criture collective sont susceptibles de produire des m&eacute;moires partag&eacute;es ? Quelle dynamique s&rsquor;&eacute;tablit alors entre les collectifs et les m&eacute;moires qu&rsquor;ils constituent ?</li>
<li><strong>&Eacute;criture et connaissance</strong> : quelle est la nature des connaissances produites collectivement ? Quelle scientificit&eacute; peut-on leur accorder au regard des processus d&rsquor;&eacute;criture qui les &eacute;laborent ?</li>
<li><strong>Communs et patrimoines</strong>: de quelles mani&egrave;res les pratiques collectives d&rsquor;&eacute;criture correspondent &agrave; des formes de soin visant &agrave; prot&eacute;ger et enrichir un patrimoine commun ? Ces dynamiques collectives g&eacute;n&egrave;rent-elles des formes hybrides de propri&eacute;t&eacute;s partag&eacute;es ?</li>
<li><strong>Autorit&eacute; et l&eacute;gitimit&eacute;</strong> : en quoi ces processus d&rsquor;&eacute;ditorialisation collaborative redistribuent les autorit&eacute;s et les l&eacute;gitimit&eacute;s traditionnelles ? Quelle place et quelles fonctions assurent alors les institutions culturelles et les chercheur&middot;e&middot;s dans ce r&eacute;gime de production des savoirs ?</li>
<li><strong>Dispositifs et gouvernance</strong> : comment s&rsquor;organisent les collectifs ainsi constitu&eacute;s ? Quels sont les modes de gouvernance qu&rsquor;ils adoptent, et quelles fonctions assument les dispositifs dans ce type de gouvernance ?</li>
</ul>
<p>Les propositions pourront porter sur :</p>
<ul>
<li>des dispositifs (institutionnels, scientifiques, associatifs, citoyens, etc.) de m&eacute;diation et de valorisation des patrimoines num&eacute;riques ou num&eacute;ris&eacute;s</li>
<li>des dispositifs d&rsquor;&eacute;ditorialisation collaborative</li>
<li>des protocoles &eacute;ditoriaux mobilisant des &eacute;critures collectives</li>
<li>des initiatives favorisant la participation et l&rsquor;innovation dans les pratiques mus&eacute;ales ou d&rsquor;archive</li>
<li>des protocoles et des outils (API) d&rsquor;ouverture de collections en lecture et/ou en &eacute;criture</li>
<li>des initiatives visant &agrave; enrichir Wikip&eacute;dia ou Wikidata</li>
<li>des collectifs d&rsquor;&eacute;criture en contexte patrimonial ou m&eacute;moriel</li>
<li>des dispositifs automatiques (bots) de valorisation de collections ou d&rsquor;archives.</li>
</ul>
<p>Cet appel vise &agrave; cr&eacute;er un processus de recherche et d&rsquor;&eacute;criture collective. Les auteurs et autrices des textes s&eacute;lectionn&eacute;s int&egrave;greront le comit&eacute; scientifique et participeront &agrave; la relecture critique des textes. Ce comit&eacute; &eacute;largi d&eacute;cidera coll&eacute;gialement des modalit&eacute;s de relecture (ouverte, semi-ouverte, publique) et se r&eacute;partira la relecture des textes soumis. Une journ&eacute;e d&rsquor;&eacute;tude viendra comme un point d&rsquor;&eacute;change et de passerelles entre les contributrices et les contributeurs.</p>
<p>Les contributions finales donneront lieu &agrave; la publication d&rsquor;un ouvrage collectif dans la collection&NonBreakingSpace;<em>Intelligences Num&eacute;riques</em>&NonBreakingSpace;aux&NonBreakingSpace;<em>Presses Universitaires de Paris Nanterre</em>, disponible en libre acc&egrave;s sur&NonBreakingSpace;<em>OpenEdition Books</em>.</p>
<h3>Bibliographie</h3>
<div id="refs">
<div id="ref-appiotti__2021">
<p>Appiotti, S&eacute;bastien, et &Eacute;va Sandri. 2021. &laquo;&NonBreakingSpace;&OpenCurlyDoubleQuote; Innovez ! Participez ! &rdquor; Interroger la relation entre mus&eacute;e et num&eacute;rique au travers des injonctions adress&eacute;es aux professionnels&NonBreakingSpace;&raquo;. <em>Culture et Mus&eacute;es</em>, n&#7506; 35 (juin):25&#8209;48. <a href="https://doi.org/10.4000/culturemusees.4383">https://doi.org/10.4000/culturemusees.4383</a>.</p>
</div>
<div id="ref-bachimont_ingenierie_2007">
<p>Bachimont, Bruno. 2007. &laquo;&NonBreakingSpace;Ing&eacute;nierie des connaissances. Le num&eacute;rique entre ontologies et documents&NonBreakingSpace;&raquo;. <em>Herm&egrave;s Lavoisier, Paris</em>.</p>
</div>
<div id="ref-beaudouin_forme_2018">
<p>Beaudouin, Val&eacute;rie. 2018. &laquo;&NonBreakingSpace;Forme des collectifs en ligne et &eacute;criture num&eacute;rique. Le cas de la m&eacute;moire de la Grande Guerre&NonBreakingSpace;&raquo;. Habilitation &agrave; diriger des recherches, Universit&eacute; Paris Descartes. <a href="https://hal.archives-ouvertes.fr/tel-02172368">https://hal.archives-ouvertes.fr/tel-02172368</a>.</p>
</div>
<div id="ref-beaudouin_web_2018">
<p>Beaudouin, Val&eacute;rie, Philippe Chevallier, et Lionel Maurel. 2018. <em>Le web fran&ccedil;ais de la Grande Guerre. R&eacute;seaux amateurs et institutionnels</em>. Presses universitaires de Paris Nanterre. <a href="https://hal.telecom-paris.fr/hal-02103837">https://hal.telecom-paris.fr/hal-02103837</a>.</p>
</div>
<div id="ref-bernard_recherche_2019">
<p>Bernard, Loup, Chlo&eacute; Besombes, Philippe Boula de Mare&uuml;il, Lisa Chupin, Erwan Dagorne, Marie Delannoy, Myriam Desainte-Catherine, et al. 2019. &laquo;&NonBreakingSpace;Recherche culturelle et sciences participatives PARTICIP-ARC&NonBreakingSpace;&raquo;. Report. Mus&eacute;um national d&rsquor;Histoire naturelle. <a href="https://hal-mnhn.archives-ouvertes.fr/mnhn-02297638">https://hal-mnhn.archives-ouvertes.fr/mnhn-02297638</a>.</p>
</div>
<div id="ref-bideran_contribuer_2020">
<p>Bideran, Jessica de, et Romain Wenz. 2020. &laquo;&NonBreakingSpace;Contribuer &agrave; la diffusion du patrimoine documentaire sur Wikip&eacute;dia : pratiques et enjeux pour les institutions culturelles&NonBreakingSpace;&raquo;. <em>Culture &amp; Mus&eacute;es. Mus&eacute;ologie et recherches sur la culture</em>, n&#7506; 35 (juin):165&#8209;88. <a href="https://doi.org/10.4000/culturemusees.4762">https://doi.org/10.4000/culturemusees.4762</a>.</p>
</div>
<div id="ref-candel_numerique_2019">
<p>Candel, Etienne, Gustavo Gomez-Mejia, Val&eacute;rie Jeanne-Perrier, et Emmanu&euml;l Souchier. 2019. <em>Le num&eacute;rique comme &eacute;criture</em>. Armand Colin. <a href="https://hal.archives-ouvertes.fr/hal-01982714">https://hal.archives-ouvertes.fr/hal-01982714</a>.</p>
</div>
<div id="ref-casemajor_pratiques_2018">
<p>Casemajor, Nathalie, Guy Bellavance, et Guillaume Sirois. 2018. &laquo;&NonBreakingSpace;Pratiques culturelles num&eacute;riques et plateformes participatives: opportunit&eacute;s, d&eacute;fis et enjeux&NonBreakingSpace;&raquo;. Rapport de recherche au programme Actions concert&eacute;es. FRQSC.</p>
</div>
<div id="ref-de_champs_testaments_2018">
<p>Champs, Emmanuelle de, Pauline Charbonnier, Florence Clavaud, Romain Dugast, Marie-Fran&ccedil;oise Limon-Bonnet, M&eacute;lisa Locatelli, Christine Nougaret, Karl Pineau, et Lucie Vieillon. 2018. &laquo;&NonBreakingSpace;&rdquor;Testaments de Poilus&rdquor;, une plate-forme participative en hommage aux Poilus morts pour la France qui met en valeur les archives notariales&NonBreakingSpace;&raquo;. <em>Le Gnomon : revue internationale d&rsquor;histoire du notariat</em>, n&#7506; 197:14&#8209;25. <a href="https://hal.archives-ouvertes.fr/hal-02076594">https://hal.archives-ouvertes.fr/hal-02076594</a>.</p>
</div>
<div id="ref-chupin_documentarisation_2016">
<p>Chupin, Lisa. 2016. &laquo;&NonBreakingSpace;Documentarisation participative et m&eacute;diation du patrimoine scientifique num&eacute;ris&eacute;. Le cas des herbiers&NonBreakingSpace;&raquo;. <em>&Eacute;tudes de communication. langages, information, m&eacute;diations</em>, n&#7506; 46 (juin):33&#8209;50. <a href="https://doi.org/10.4000/edc.6499">https://doi.org/10.4000/edc.6499</a>.</p>
</div>
<div id="ref-coriat_retour_2015">
<p>Coriat, Benjamin. 2015. <em>Le retour des communs. La crise de l&rsquor;id&eacute;ologie propri&eacute;taire</em>. &Eacute;ditions Les Liens qui lib&egrave;rent.</p>
</div>
<div id="ref-crozat_elements_2012">
<p>Crozat, St&eacute;phane, Bruno Bachimont, Isabelle Cailleau, Serge Bouchardon, et Ludovic Gaillard. 2012. &laquo;&NonBreakingSpace;&Eacute;l&eacute;ments pour une th&eacute;orie op&eacute;rationnelle de l&rsquor;&eacute;criture num&eacute;rique&NonBreakingSpace;&raquo;. <em>Document num&eacute;rique</em> 14 (3):9&#8209;33. <a href="http://www.cairn.info/revue-document-numerique-2011-3-p-9.htm">http://www.cairn.info/revue-document-numerique-2011-3-p-9.htm</a>.</p>
</div>
<div id="ref-juanals_strategies_2020">
<p>Juanals, Brigitte, et Jean-Luc Minel. 2020. &laquo;&NonBreakingSpace;Strat&eacute;gies &eacute;ditoriales des mus&eacute;es. Une approche de la m&eacute;diation par l&rsquor;acc&egrave;s ouvert aux donn&eacute;es num&eacute;ris&eacute;es&NonBreakingSpace;&raquo;. <em>Culture &amp; Mus&eacute;es. Mus&eacute;ologie et recherches sur la culture</em>, n&#7506; 35 (juin):49&#8209;75. <a href="https://doi.org/10.4000/culturemusees.4427">https://doi.org/10.4000/culturemusees.4427</a>.</p>
</div>
<div id="ref-mellet_penser_2020">
<p>Mellet, Margot. 2020. &laquo;&NonBreakingSpace;Penser le palimpseste num&eacute;rique. Le projet d&rsquor;&eacute;dition num&eacute;rique collaborative de l&rsquor;Anthologie palatine&NonBreakingSpace;&raquo;. <em>Captures: Figures, th&eacute;ories et pratiques de l&rsquor;imaginaire</em> 5 (1). <a href="https://doi.org/10.7202/1073479ar">https://doi.org/10.7202/1073479ar</a>.</p>
</div>
<div id="ref-merzeau_intelligence_2013">
<p>Merzeau, Louise. 2013a. &laquo;&NonBreakingSpace;L&rsquor;intelligence des traces&NonBreakingSpace;&raquo;. <em>Intellectica - La revue de l&rsquor;Association pour la Recherche sur les sciences de la Cognition (ARCo)</em> 1 (59):p.115&#8209;135. <a href="https://halshs.archives-ouvertes.fr/halshs-01071211">https://halshs.archives-ouvertes.fr/halshs-01071211</a>.</p>
</div>
<div id="ref-merzeau_editorialisation_2013">
<p>Merzeau, Louise. 2013b. &laquo;&NonBreakingSpace;&Eacute;ditorialisation collaborative d&rsquor;un &eacute;v&eacute;nement&NonBreakingSpace;&raquo;. <em>Communication et organisation</em>, n&#7506; 43 (juin):105&#8209;22. <a href="https://doi.org/10.4000/communicationorganisation.4158">https://doi.org/10.4000/communicationorganisation.4158</a>.</p>
</div>
<div id="ref-moirez_construire_2017">
<p>Moirez, Pauline. 2017. <em>Construire et enrichir ensemble les savoirs : crowdsourcing et patrimoine num&eacute;ris&eacute;</em>. &Eacute;ditions du Cercle de la Librairie. <a href="https://www.cairn.info/communs-du-savoir-et-bibliotheques--9782765415305-page-127.htm">https://www.cairn.info/communs-du-savoir-et-bibliotheques--9782765415305-page-127.htm</a>.</p>
</div>
<div id="ref-navarro_mediation_2019">
<p>Navarro, Nicolas, et Lise Renaud. 2019. &laquo;&NonBreakingSpace;La m&eacute;diation num&eacute;rique au mus&eacute;e en proc&egrave;s&NonBreakingSpace;&raquo;. <em>Revue fran&ccedil;aise des sciences de l&rsquor;information et de la communication</em>, n&#7506; 16 (mai). <a href="https://doi.org/10.4000/rfsic.5592">https://doi.org/10.4000/rfsic.5592</a>.</p>
</div>
<div id="ref-sauret_revue_2020">
<p>Sauret, Nicolas. 2020. &laquo;&NonBreakingSpace;De la revue au collectif : la conversation comme dispositif d&rsquor;&eacute;ditorialisation des communaut&eacute;s savantes en lettres et sciences humaines&NonBreakingSpace;&raquo;. Th&egrave;se de doctorat, Universit&eacute; de Nanterre - Paris X ; Universit&eacute; de Montr&eacute;al. <a href="https://tel.archives-ouvertes.fr/tel-03267857">https://tel.archives-ouvertes.fr/tel-03267857</a>.</p>
</div>
<div id="ref-severo_imperatif_2021">
<p>Severo, Marta. 2021. <em>L&rsquor;imp&eacute;ratif participatif. Institutions culturelles, amateurs et plateformes</em>. &Eacute;tudes &amp; controverses. INA.</p>
</div>
<div id="ref-strzelichowska_europeana_2021">
<p>Strzelichowska, Aleksandra. 2021. &laquo;&NonBreakingSpace;Europeana y GIF IT UP fomentan la creatividad en tiempos de pandemia&NonBreakingSpace;&raquo;. <em>PH: Bolet&iacute;n del Instituto Andaluz del Patrimonio Hist&oacute;rico</em> 29 (102):6&#8209;7. <a href="https://dialnet.unirioja.es/servlet/articulo?codigo=7799407">https://dialnet.unirioja.es/servlet/articulo?codigo=7799407</a>.</p>
</div>
</div>

### Modalités de soumission

L'appel s'inscrit dans une démarche interdisciplinaire, les propositions peuvent émaner de chercheur·e·s et de professionnel·e·s travaillant dans tout domaine. L'appel est ouvert aux approches théoriques ainsi qu'aux retours d'expérience et travaux en cours.

Les propositions doivent tenir entre 10&nbsp;000 et 30&nbsp;000 caractères et être envoyées au comité d'organisation ([fplab@liste.parisnanterre.fr][mailfplab]) avant le 28 février 2022. Les auteur·e·s sont invité·e·s à contacter le comité d'organisation en amont pour vérifier la cohérence de la proposition avec l'appel. Tous les textes envoyés, cohérents avec les objectifs de l'appel, seront retenus et feront l'objet d'un processus de relecture collective réalisé par un comité scientifique constitué par l'ensemble des auteur·e·s.

Les présentations lors de la journée n'excéderont pas 20 minutes, afin de laisser une part importante du temps aux échanges entre participant·e·s.

Les textes finaux pourront faire entre 20&nbsp;000/25&nbsp;000 et 35&nbsp;000 caractères et devront être envoyés avant le 30 juin 2022.

La journée d'étude est organisée par le FabPart Lab dans le cadre du labex _Les passés dans le présent_ et a donc bénéficié de l’aide de l’État géré par l’ANR au titre du programme _Investissements d’avenir_ portant la référence ANR-11-LABX-0026-01.

![logoLabex](https://i.imgur.com/Ns4NJVb.png){: style="width:100px;"} &nbsp;&nbsp;&nbsp; ![logoFPL](https://i.imgur.com/ZGvzoEe.png){: style="width:100px;"}

### Envoi des propositions

Les propositions sont à envoyer avant le 28 février 2022 à minuit à [fplab@liste.parisnanterre.fr][mailfplab].

Vous êtes invité à vérifier la pertinence de vos propositions avant soumission avec le comité d'organisation.


### Calendrier

  <table id="calendarje" style="border:1px solid black">
  <colgroup>
  <col style="width: 24%" />
  <col style="width: 75%" />
  </colgroup>
  <tbody>
  <tr class="odd">
  <td style="text-align: left;">28 f&eacute;vrier 2022</td>
  <td style="text-align: left;">Soumission d&rsquor;une premi&egrave;re version du texte (10&NonBreakingSpace;000 à 20&NonBreakingSpace;000 car.) </td>
  </tr>
  <tr class="even">
  <td style="text-align: left;">15 mars 2022</td>
  <td style="text-align: left;">Lancement du processus de relecture</td>
  </tr>
  <tr class="odd">
  <td style="text-align: left;">14 avril 2022</td>
  <td style="text-align: left;">Journ&eacute;e d&rsquor;&eacute;tude</td>
  </tr>
  <tr class="even">
  <td style="text-align: left;">30 juin 2022</td>
  <td style="text-align: left;">Soumission des textes finaux (20&NonBreakingSpace;000 à 35&NonBreakingSpace;000 car.)</td>
  </tr>
  </tbody>
  </table>


### Comité d'organisation

- Claude Mussou (INA Thèque)
- Nicolas Sauret (FabPart Lab, Université Paris Nanterre)
- Marta Severo (FabPart Lab, Université Paris Nanterre)

**Contacter le comité**: [fplab@liste.parisnanterre.fr][mailfplab]

### Comité scientifique

Le comité scientifique sera constitué de tous les auteur·e·s des textes sélectionnés.

[mailfplab]: mailto:fplab@liste.parisnanterre.fr
