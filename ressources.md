---
layout: page
background: darker
title: Ressources du FabPart Lab
date: 2021-09-13
author:
categories: ateliers
tags:
lead_text: C'est un lead texte !
custom-css-list: /assets/css/ateliers.css
---

Le FabPart Lab rassemble progressivement une base de ressources à partir des ateliers passés. Chaque atelier a fait l'objet d'un dépôt sur la plateforme [Nakala](https://www.nakala.fr), comprenant la vidéo enregistrée et la prise de note collaborative. Les données sont rééditorialisées ici comme ressources, ou disponible directement depuis [la collection Nakala : 10.34847/nkl.45abmm0o](https://nakala.fr/u/collections/10.34847/nkl.45abmm0o).

<!-- ateliers Grid -->
  <div class="container" id="ateliers">
  <!-- pourra être utile si on sépare les ateliers pratiques, théoriques ou événements -->
  <!--   <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">{{ site.data.sitetext.ateliers.title | markdownify | default: "Ateliers" }}</h2>
        <h3 class="section-subheading text-muted">{{ site.data.sitetext.ateliers.text | markdownify }}</h3>
      </div>
    </div> -->
    <div id='archives'>
    <h2>Saison des ateliers 2020-2021</h2>
    <div class="row">
  {% assign ateliers_by_date = site.ateliers | where: "infos.etat", "archive" | sort: "infos.date" | reverse %}
	{% for atelier in ateliers_by_date %}
      <div class="col-md-4 col-sm-6 portfolio-item event-item">
        <a href="{{ atelier.url }}">
          <img class="img-fluid" src="{{ atelier.image }}" alt="atelier.alt">
        </a>
        <div class="portfolio-caption">
          <h4><a href="{{ atelier.url }}">{{ atelier.title }}</a></h4>
          <h5><a href="{{ atelier.url }}"><em>{{ atelier.subtitle }}</em></a></h5>
          <p class="text-muted">{{ atelier.type }}</p>
          <!-- <p class="text-muted">{{ atelier.caption.date | date_to_french }}</p> -->
          <p class="text-muted">
            {% assign d = atelier.infos.date | date: "%-d" %}
            {% assign m = atelier.infos.date | date: "%-m" %}
            {% case d %}
              {% when '1' %}1er
              {% else %}{{ d }}
            {% endcase %}
            {% case m %}
              {% when '1' %}jan.
              {% when '2' %}fév.
              {% when '3' %}mars
              {% when '4' %}avr.
              {% when '5' %}mai
              {% when '6' %}juin
              {% when '7' %}juil.
              {% when '8' %}août
              {% when '9' %}sept.
              {% when '10' %}oct.
              {% when '11' %}nov.
              {% when '12' %}déc.
            {% endcase %}
            {{ atelier.infos.date | date: "%Y" }}
          </p>
        </div>
      </div>
	{% endfor %}
    </div>
    </div>
    <hr/>
  </div>
{% include modals.html %}



<!--
Le FabPart Lab rassemble progressivement une base de connaissances d'outils et de méthodologies numériques en SHS. Les ressources sont distribuées selon les phases d'un projet de recherche, [telles qu'adoptées par la TGIR Huma-Num](https://www.huma-num.fr/les-services-par-etapes/).

![phases recherche](/assets/img/hn-phases-recherche.png)
-->
