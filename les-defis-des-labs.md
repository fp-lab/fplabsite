---
layout: page
background: lighter
title: Comment accompagner les humanités numériques&nbsp;?  Les défis des Labs
subtitle:
date: 2022-05-18
author:
categories:
tags:
lead_text:
custom-css-list:
---

<style>
  h3 {
    margin: 1.7em 0 1em;
  }
  :target::before {
    content: "";
    display: block;
    height: 130px; /* fixed header height*/
    margin: -130px 0 0; /* negative fixed header height */
  }
</style>

### Humanistica 2022 - Session de travail

<div style="width:40%;float:right; margin-left:1em;">
<iframe src="/assets/pdf/Humanistica2022_proposition_FPLAB-invitation.pdf" width="100%" height="500px">
</iframe>
<p style="float:right; font-size: smaller;">
<a href="/assets/pdf/Humanistica2022_proposition_FPLAB-invitation.pdf">Ouvrir le pdf</a>
</p>
</div>

<p>Le <a href="https://fplab.parisnanterre.fr">FabPart Lab</a> du Labex <a href="https://passes-present.eu">Les Pass&eacute;s dans le pr&eacute;sent</a> (Universit&eacute; Paris Nanterre) organise un groupe de travail consacr&eacute; aux initiatives d&rsquor;accompagnement &agrave; la recherche en humanit&eacute;s num&eacute;riques. Sous des d&eacute;nominations diff&eacute;rentes (DH Lab, M&eacute;dialab, Data Lab, centre d&rsquor;appui, cellule de soutien, etc.), plusieurs structures et entit&eacute;s ont &eacute;t&eacute; cr&eacute;&eacute;es r&eacute;cemment au sein des universit&eacute;s, des institutions de recherche ou des institutions culturelles. Ces initiatives tentent de r&eacute;pondre aux besoins croissants des chercheur&middot;e&middot;s et des &eacute;quipes de recherche dans les diff&eacute;rentes disciplines en sciences humaines et sociales pour ma&icirc;triser les bonnes pratiques num&eacute;riques en termes de gestion et de valorisation des donn&eacute;es de la recherche et pour s&rsquor;approprier les m&eacute;thodologies num&eacute;riques de traitement, d&rsquor;analyse et d&rsquor;&eacute;ditorialisation de ces donn&eacute;es.</p>
  <p>Or, les communaut&eacute;s en humanit&eacute;s num&eacute;riques se sont engag&eacute;es tr&egrave;s t&ocirc;t sur les probl&eacute;matiques de litt&eacute;ratie num&eacute;rique et d&rsquor;ouverture de la recherche, aujourd&rsquor;hui largement prises en compte au niveau institutionnel. De ce point de vue, ces entit&eacute;s d&rsquor;accompagnement &agrave; la recherche &eacute;largissent ces pr&eacute;occupations initialement exprim&eacute;es par la communaut&eacute; des humanit&eacute;s num&eacute;riques &agrave; d&rsquor;autres domaines des sciences humaines et sociales. Il nous semble alors pertinent d&rsquor;initier un dialogue entre ces initiatives, sp&eacute;cifiquement sous l&rsquor;&eacute;gide des humanit&eacute;s num&eacute;riques, pour b&eacute;n&eacute;ficier de l&rsquor;exp&eacute;rience de la communaut&eacute; en la mati&egrave;re.</p>
  <p>Pour cette session de travail, nous invitons plusieurs structures de type DH Lab, Data Lab ou toute cellule de soutien au num&eacute;rique d&eacute;di&eacute;e &agrave; la recherche en SHS, afin d&rsquor;ouvrir un espace d&rsquor;&eacute;change sur leur exp&eacute;rience, leurs difficult&eacute;s et leurs besoins. La session de travail sera l&rsquor;occasion pour chaque entit&eacute; de pr&eacute;senter sa philosophie, ses ressources et son public (type de projets, type de besoins), mais aussi de pointer les difficult&eacute;s rencontr&eacute;es. L&rsquor;un des objectifs de la session sera d&rsquor;identifier les obstacles r&eacute;currents et de d&eacute;gager des pistes communes d&rsquor;am&eacute;lioration.</p>
  <p><strong>Axes de r&eacute;flexion&NonBreakingSpace;:</strong></p>
  <ul>
  <li>Bases de ressources&NonBreakingSpace;: structuration, recensement, mutualisation</li>
  <li>Temporalit&eacute; et espace de l&rsquor;accompagnement</li>
  <li>Labs et infrastructures&NonBreakingSpace;: comment adosser aux infrastructures existantes&NonBreakingSpace;?</li>
  <li>Ancrage institutionnel : quel attachement aux institutions&NonBreakingSpace;?</li>
  <li>Litt&eacute;ratie num&eacute;rique&NonBreakingSpace;: accompagner pour transmettre</li>
  </ul>
  <p><strong>Format&NonBreakingSpace;:</strong></p>
  <ul>
  <li>Dur&eacute;e du groupe de travail&NonBreakingSpace;: 2h ou 2,5h</li>
  <li>Cr&eacute;neau pressenti&NonBreakingSpace;: <em>Sessions de travail</em> pr&eacute;vues le 18 mai 2022 en matin&eacute;e</li>
  </ul>


### Organisation

- Nicolas Sauret (FabPart Lab, Université Paris Nanterre)
- Marta Severo (FabPart Lab, Université Paris Nanterre)

**Contacter le comité**: [fplab@liste.parisnanterre.fr][mailfplab]

_Cette session de travail est organisée par le FabPart Lab dans le cadre du colloque [Humanistica 2022](https://humanistica2022.sciencesconf.org/). Le FabPart Lab est soutenu par le labex Les passés dans le présent et a donc bénéficié de l’aide de l’État géré par l’ANR au titre du programme Investissements d’avenir portant la référence ANR-11-LABX-0026-01._

![logoLabex](https://i.imgur.com/Ns4NJVb.png){: style="width:100px;"} &nbsp;&nbsp;&nbsp; ![logoFPL](https://i.imgur.com/ZGvzoEe.png){: style="width:100px;"}


[mailfplab]: mailto:fplab@liste.parisnanterre.fr
