---
layout: page
background: darker
title: Événements du FabPart Lab
date: 2020-12-10
author:
categories: ateliers
tags:
lead_text: C'est un lead texte !
custom-css-list: /assets/css/ateliers.css
---

<!-- ateliers Grid -->
  <div class="container" id="ateliers">
  <!-- pourra être utile si on sépare les ateliers pratiques, théoriques ou événements -->
  <!--   <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">{{ site.data.sitetext.ateliers.title | markdownify | default: "Ateliers" }}</h2>
        <h3 class="section-subheading text-muted">{{ site.data.sitetext.ateliers.text | markdownify }}</h3>
      </div>
    </div> -->
    <div id="coming">
    <h2>Programme 2021-2022</h2>
    <div class="row">
  {% assign ateliers_coming = site.ateliers | where: "infos.etat", "coming" | sort: "infos.date" %}
	{% for atelier in ateliers_coming %}
      <div class="col-md-4 col-sm-6 portfolio-item event-item">
        <a href="{{ atelier.url }}">
          <img class="img-fluid" src="{{ atelier.image }}" alt="atelier.alt">
        </a>
        <div class="portfolio-caption">
          <h4><a href="{{ atelier.url }}">{{ atelier.title }}</a></h4>
          <h5><a href="{{ atelier.url }}"><em>{{ atelier.subtitle }}</em></a></h5>
          <p class="text-muted">{{ atelier.type }}</p>
          <!-- <p class="text-muted">{{ atelier.caption.date | date_to_french }}</p> -->
          <p class="text-muted">
            {% assign d = atelier.infos.date | date: "%-d" %}
            {% assign m = atelier.infos.date | date: "%-m" %}
            {% case d %}
              {% when '1' %}1er
              {% else %}{{ d }}
            {% endcase %}
            {% case m %}
              {% when '1' %}jan.
              {% when '2' %}fév.
              {% when '3' %}mars
              {% when '4' %}avr.
              {% when '5' %}mai
              {% when '6' %}juin
              {% when '7' %}juil.
              {% when '8' %}août
              {% when '9' %}sept.
              {% when '10' %}oct.
              {% when '11' %}nov.
              {% when '12' %}déc.
            {% endcase %}
            {{ atelier.infos.date | date: "%Y" }}
          </p>
        </div>
      </div>
	{% endfor %}
    </div>
    </div>
  </div>

Retrouvez les saisons précédentes dans [les ressources du FabPart Lab](ressources.html).
{% include modals.html %}
