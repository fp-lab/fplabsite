# Site web du FabPart Lab


Structure des contenus:

- `_data/` : contient les données du site et les contenus de la home
- `_ateliers/` : collection des événements
- `actions/` : ces pages détaillent les modes d'action du FPLab
- `_ressources` : collection de ressources

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/) Les contenus du site sont sous licence CC-BY-SA 4.0.

Site réalisé avec Jekyll sur le theme [Agency](https://github.com/raviriley/agency-jekyll-theme) (MIT licence), auquel ont été ajoutés des collections et un blog.
