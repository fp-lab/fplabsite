---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: INA
subtitle: Faire de la recherche avec les archives audiovisuelles
tags: [archives, collections audiovisuelles, INA, données de la recherche]
image: /assets/img/event/INA.jpg
alt: Archives INA
infos:
  date: 2021-05-03
  horaire: 14h-15h30
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.adf5t966
  video1: 10.34847/nkl.adf5t966/f632af29e5ca63aaeeef03aad1fc7531b3e22c3c
  video2: 10.34847/nkl.adf5t966/dff3e6d6ae1725e9c15ea4f2b0c66a9235eb5a32
notes: atelier-ina-minutes_20210503.md
---

#### Présentation
L'interdisciplinarité, les dynamiques exploratoires et expérimentales sont des leviers essentiels du renouvellement des pratiques de recherche. Au travers de projets orientés «&nbsp;données&nbsp;» et «&nbsp;outillage&nbsp;» de la recherche, cette séance présentera de nouvelles approches et instrumentations pour l'exploitation scientifique des collections audiovisuelles et numériques de l'INA.

&nbsp;

#### Vidéos

**Première partie**
{: style="margin-top: 2em;"}

{% if page.nakala %}
<video width="100%" style="max-width: 700px" controls preload="auto" >
  <source src="https://api.nakala.fr/data/{{ page.nakala.video1 }}" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo.
</video>
<p class="videocaption"><a href="https://nakala.fr/{{ page.nakala.doi }}">Voir la donnée sur nakala.fr</a></p>
{% endif %}

**Seconde partie**
{: style="margin-top: 2em;"}

{% if page.nakala %}
<video width="100%" style="max-width: 700px" controls preload="auto" >
  <source src="https://api.nakala.fr/data/{{ page.nakala.video2 }}" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo.
</video>
<p class="videocaption"><a href="https://nakala.fr/{{ page.nakala.doi }}">Voir la donnée sur nakala.fr</a></p>
{% endif %}

&nbsp;



#### Déroulé de la séance

**Claude Mussou** (Responsable de l'Ina Thèque) : Introduction

**Laetitia Larcher** (chargée de mission documentation à l'INA) : Les sources disponibles pour la recherche

[**Cecile Meadel**](https://www.u-paris2.fr/fr/universite/enseignants-chercheurs/mme-cecile-meadel) et [**David Doukan**](https://www.linkedin.com/in/david-doukhan-588074107/?originalSubdomain=fr) : _Gender Equality Monitor_, égalité et parité Hommes/Femmes dans les médias

> Le projet [Gender Equality Monitor](https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-19-ce38-0012/?tx_anrprojects_funded%5Bcontroller%5D=Funded&cHash=335d9f79f322297dc707fb5c476a9b08), financé par l’ANR, porte sur l’étude des différences de représentation et traitement des femmes et des hommes dans les médias audiovisuels. Fondé sur un corpus de documents échantillonnés sur une période de plus de 80 ans, il mobilise des chercheurs en intelligence artificielle (analyse du signal, TAL…) ainsi que des équipes de recherche dans les domaines des STIC et des SHS. L’articulation de méthodes quantitatives et qualitatives devrait aboutir à des résultats susceptibles d’éclairer le débat public sur les enjeux d’égalité et de parité Hommes/Femmes.

[**Matteo Treleani**](https://fr.wikipedia.org/wiki/Matteo_Treleani) et [**Olivier Buisson**](https://www.linkedin.com/in/olivier-buisson-60a2387/?originalSubdomain=fr) :  _Crossing Borders Archives_ et l'outil Snoop, quelles images symboliques de l’Europe dans les médias audiovisuels&nbsp;?

> Le projet ANR [CROBORA](https://anr-crobora.fr/) souhaite tracer les réutilisations d’archives audiovisuelles dans les récits médiatiques portant sur la construction de l’Union Européenne. CROBORA fait l’hypothèse que les logiques circulatoires à l’œuvre dans la médiatisation des images du passé contribuent à la constitution d’une mémoire européenne construite autour d’un nombre limité d’images symboliques. Le projet s’appuie sur des corpus d’archives de l’INA et de la Rai ainsi que sur des outils développés à l’INA de détection d’images similaires .

[**Jean Carrive**](https://www.linkedin.com/in/jeancarrive/?originalSubdomain=fr) et [**Franck Mazuet**](https://antract.hypotheses.org/29) :  _ANTRACT_, un corpus historique à l'épreuve de la transdisciplinarité.

> [ANTRACT](https://antract.hypotheses.org/category/ina) est un projet financé par l’ANR. Il porte sur l’analyse de la collection des Actualités Françaises conservée à l’INA. Ce corpus de presse filmée qui couvre une période de près de 25 années n’a jamais fait l’objet d’une analyse systématique. Dans une approche transdisciplinaire, le projet propose de nouvelles modalités et pratiques de recherche appliquées à ce corpus historique. Au service de questions de recherche en histoire sociale, politique ou culturelle, cette approche se fonde sur des outils innovants d’analyse de l’image et du son, de transcription automatique de la parole ou de textométrie.

&nbsp;
