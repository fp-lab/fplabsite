---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Claire Scopsi
subtitle: Documentation et valorisation des témoignages
tags: [témoignage, corpus, archives de la parole, documentation, entretiens]
image: /assets/img/event/Scopsi.jpg
alt: Claire Scopsi
infos:
  date: 2021-04-12
  horaire: 14h-15h30
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.97d2rinc
  video: 10.34847/nkl.97d2rinc/85c3c8411e3996f9b3ae93e9646bc8471b3a7063
notes: atelier-temoignage-minutes_20210412.md
---

#### Présentation

La pratique des entretiens enregistrés et filmés est déjà ancienne (en France, les archives de la parole datent du début du siècle). Les enregistrements de témoins sont effectués par des différents acteurs dans des circonstances diverses, sans que les différences soient toujours très perceptibles. En effet, au-delà du dispositif technico-spatial et de la thématique de l'entretien, ce sont les statuts respectifs du "témoignaire" et du témoin, ainsi que les objectifs visés qui déterminent la préparation, le déroulement, le traitement et le devenir des entretiens.

Plusieurs types de contextes de témoignage révèlent différentes méthodologies&nbsp;: le documentaire, le récit de vie, l'entretien en histoire contemporaine, en anthropo-sociologie ou en linguistique.

Claire Scopsi est MCF HDR en sciences de l'information et de la communication au CNAM (Paris). Elle est co-responsable de la thématique "Éditorialisation, patrimones, autorité" du laboratoire Dicen-IdF.
