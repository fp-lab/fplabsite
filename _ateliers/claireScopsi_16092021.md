---
layout: atelier
#what displays when the item is clicked:
type: Atelier pratique
title: Claire Scopsi
subtitle: >-
  Méthodologie de l'entretien #1 Collecter les données pérennes
tags: [témoignage, corpus, archives de la parole, documentation, entretiens]
image: https://cdn.britannica.com/70/148070-050-754B12B5/tape-recorder.jpg
infos:
  date: 2021-09-16
  horaire: 9h30-11h
  lieu: salle Séminaire 2 - Bâtiment Max Weber (U. Paris Nanterre)
  online: true
  etat: archive
nakala:
  doi: 10.34847/nkl.104ae93q
  video: 10.34847/nkl.104ae93q/6b89c6ad7f4d25ce5bc746f40e541115375ee97e
---

#### Présentation

Cet atelier pratique s'inscrit à la suite de l'atelier théorique [«&nbsp;Documentation et valorisation des témoignages&nbsp;»][theorique], donné par Claire Scopsi à la session précédente. Il a été suivi par un second atelier pratique sur les méthodologies de l'entretien&nbsp;: [«&nbsp;L’analyse chrono-thématique, une alternative à la transcription&nbsp;» (30 sept. 2021)][pratique2].

> L’entretien semi-directif est une pratique au centre des méthodes qualitatives. Il permet de recueillir des informations originales et précieuses qui ne figurent pas dans les sources écrites ni dans les fichiers de données. Pourtant les entretiens collectés dans le cadre de recherches finissent trop souvent dans un tiroir avant de disparaître dans une panne de disque dur, ou sur un support illisible.
>
> Ce cours aborde trois livrables à produire avant de lancer la campagne d’entretiens, afin d’optimiser l’exploitation et la valorisation des entretiens collectés.
>
> 1. le dossier préparatoire. Il contextualise chaque entretien, recueille des informations sur le témoin, prévoit les conditions d’enregistrement, le guide d’entretien et les méthodes d’analyse des données recueillies&nbsp;;
> 2. le contrat de diffusion. Il permet d’informer le témoin de ses droits et de son statut à l’égard de l’enregistrement effectué. Il permet également au chercheur d’envisager au préalable tout ce qu’il souhaite faire des captations et de s’assurer de pouvoir les utiliser en toute légalité&nbsp;;
> 3. le Plan de Gestion des données. Il permet d’envisager les lieux de stockage et les opérations à mener dans le futur pour conserver les enregistrements.

Claire Scopsi est MCF HDR en sciences de l'information et de la communication au CNAM (Paris). Elle est co-responsable de la thématique "Éditorialisation, patrimones, autorité" du laboratoire Dicen-IdF.

[theorique]: claireScopsi_12042021.html
[pratique2]: claireScopsi_30092021.html
