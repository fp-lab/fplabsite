---
layout: atelier
#what displays when the item is clicked:
type: Atelier découverte
title: Permanence
subtitle: Collecte de données
tags: [permanence, participation, découverte, numérique]
image: /assets/img/illustr/collecte-43.png
alt: Collecte
infos:
  date: 2021-03-01
  horaire: 12h-14h
  lieu: en ligne
  etat: archive
---

<video width="65%" style="margin: 0 15%;" controls>
  <source src="/assets/video/ateliercollecte.mp4" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo.
</video>

&nbsp;

**La séance du 1er mars était dédiée à la phase de collecte de données.** L'atelier s'est appuyé sur les retours d'expérience de projets de recherche :

- [le projet de Christophe Tuffery](https://inp.hypotheses.org/1391), doctorant à l'Institut National du Patrimoine. Son intervention s'intitule _«&nbsp;Un projet de transcription numérique de carnets archéologiques de terrain : réflexions préalables sur les sources de données&nbsp;»_
- le projet de master [_Dessine moi un cartel_](https://severinemillotd.wixsite.com/dmuc/blank) de Séverine Millot-Dumazert, étudiante en Master 2 à ArTec
- le projet [_Vitrines en confinement_](https://vitrinesenconfinement.huma-num.fr/) de Marta Severo et de Sarah Gensburger (le projet fera finalement l'objet d'une autre séance)

&nbsp;

---

&nbsp;

Le FabPart Lab propose chaque lundi une permanence de 12h à 14h [en ligne]({{ site.data.sitetext.urls.videoconf }}), en attendant que la situation sanitaire permette un accueil en présentiel.

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.
