---
layout: atelier
#what displays when the item is clicked:
type: Atelier découverte
title: Permanence
subtitle: A l'écoute de vos projets
tags: [permanence, participation, découverte, numérique]
image: /assets/img/event/weber-corridor.jpeg
alt: Bâtiment Max Weber
infos:
  date: 2021-03-08
  horaire: 12h-14h
  lieu: online
  etat: archive
---


Le FabPart Lab propose chaque lundi une permanence de 12h à 14h dans le bureau M106 (bâtiment _Éphémère 1_) ou [en ligne]({{ site.data.sitetext.urls.videoconf }}).

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.

Vous pouvez également demander un rendez-vous individuel (en présentiel ou en distanciel) pour discuter d'un projet, obtenir des conseils, ou évoquer [un accompagnement sprint](/actions/sprint.html). Écrivez-nous à [{{ site.data.sitetext.urls.mailcontact }}](mailto:{{ site.data.sitetext.urls.mailcontact }}).

Plus d'information sur [la permanence](/actions/decouverte.html).
