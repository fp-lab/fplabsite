---
layout: atelier
#what displays when the item is clicked:
type: Atelier pratique
title: Claire Scopsi
subtitle: >-
  Méthodologie de l'entretien #2 L’analyse chrono-thématique, une alternative à la transcription
tags: [témoignage, corpus, archives de la parole, documentation, entretiens, transcription]
image: /assets/img/event/transcript.png
infos:
  date: 2021-09-30
  horaire: 9h30-11h
  lieu: salle Séminaire 2 - Bâtiment Max Weber (U. Paris Nanterre)
  online: true
  etat: archive
nakala:
  doi: 10.34847/nkl.a0b9tc8a
  video: 10.34847/nkl.a0b9tc8a/ca158ea76f56ff15a981f871e7ab9ceb2723b671
---

#### Présentation

Cet atelier pratique s'inscrit à la suite de l'atelier théorique [«&nbsp;Documentation et valorisation des témoignages&nbsp;»][theorique] et de l'atelier pratique [«&nbsp;Collecter des données pérennes&nbsp;»][pratique1], donnés par Claire Scopsi.

> À l’heure où les enregistrements numériques rendent de plus en plus facile l’accès aux entretiens collectés et leur manipulation, nous nous demanderons si la transcription _in extenso_ des entretiens est toujours indispensable. L’atelier se déroulera en 3 temps&nbsp;:
>
> 1. Un état des lieux de la transcription&nbsp;: quand est-elle indispensable&nbsp;? Combien de temps  faut-il pour transcrire&nbsp;? Quelles sont les performances et les limites des logiciels de transcription automatiques&nbsp;?.
> 2. L’analyse chrono-thématique&nbsp;: présentation et commentaire d’une fiche type, permettant à la fois de contextualiser l’entretien et d’en décrire les supports, et de ménager des points d’accès au contenu.
> 3. Démonstration et prise en main du logiciel Annotate-Chrono développé au laboratoire Dicen-IdF, pour faciliter la rédaction des analyses chrono-thématiques.

Claire Scopsi est MCF HDR en sciences de l'information et de la communication au CNAM (Paris). Elle est co-responsable de la thématique "Éditorialisation, patrimones, autorité" du laboratoire Dicen-IdF.

[theorique]: claireScopsi_12042021.html
[pratique1]: claireScopsi_16092021.html
