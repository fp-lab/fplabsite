---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Cyril Isnart
subtitle: La participation en pratique. Une généalogie intellectuelle et deux expériences
tags: [participation, pratique participative, musée]
image: /assets/img/event/jeparticipe.png
alt: Cyril Isnart
infos:
  date: 2021-06-07
  horaire: 14h-15h30
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.b8d3e4gq
  video: 10.34847/nkl.b8d3e4gq/8fcc1afc5bad8522a7bf9e58aaac852b198ef31a
notes: atelier-participation-minutes_20210607.md
---


#### Présentation
À travers un retour sur l'apparition de la participation dans son parcours de recherche et l'exposition de deux expériences récentes prennant la participation au mot, [Cyril Isnart](https://www.cnrs.fr/fr/personne/cyril-isnart) nous a proposé une lecture critique et engagée du concept et de la pratique de la participation.

Il a dans un premier temps exposé comment la notion s'est imposée, dans les années 2000, aux milieux patrimoniaux et académiques dans lesquels il travaillait. Il a ensuite présenté les enseignements tirés d'un atelier interne du Mucem, mené en 2020 et consacré aux pratiques participatives et collaboratives dans les musées de société, ainsi qu'un programme de recherche qui met la participation au travail, de manière littérale et réflexive. Ces trois lignes, personnelles et peut-être arbitraires, ont permis de poser quelques principes sur la participation en pratique, en dégageant certaines forces et certaines difficultés qui caractérisent les usages de la notion.

Cyril Isnart est anthropologue au sein de l’[Institut d’ethnologie méditerranéenne, européenne et comparative](https://www.idemec.cnrs.fr/) à Aix-en-Provence, spécialiste des résurgences patrimoniales de traditions musicales et religieuses en Europe du sud et en Méditerranée, et au Mucem, Département Recherche et Enseignement.
