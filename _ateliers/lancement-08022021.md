---
layout: atelier
#what displays when the item is clicked:
type: Événement
title: Lancement du FabPart Lab
subtitle:
tags: [permanence, participation, découverte, numérique]
image: /assets/img/event/weber-corridor.jpeg
alt: Bâtiment Max Weber
infos:
  date: 2021-02-08
  horaire: 11h30-14h
  lieu: online
  etat: archive
---


Le FabPart Lab se lance ! [Rendez-vous en ligne]({{ site.data.sitetext.urls.videoconf }}) pour une présentation de ses activités.

**Au programme :**
- Introduction par Ghislaine Glasson Deschaumes et Emmanuel Grimaud
- Quelques mots sur la philosophie du projet par Claude Mussou et  Marta Severo
- Présentation des modalités d'action et d'accompagnement du FabPart Lab
- Echange avec les participants sur les besoins en termes de contribution et de participation

#### Enregistrement (8 février 2021)

 <video width="65%" controls>
  <source src="/assets/video/lancement.mp4" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo.
</video>

#### Présentation

<iframe width="65%" height="280em" src="https://fp-lab.gitpages.huma-num.fr/presentation/lancement-8fev2021/"></iframe>

---

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.

Vous pouvez également demander un rendez-vous individuel (en présentiel ou en distanciel) pour discuter d'un projet, obtenir des conseils, ou évoquer [un accompagnement sprint](/actions/sprint.html). Écrivez-nous à [{{ site.data.sitetext.urls.mailcontact }}](mailto:{{ site.data.sitetext.urls.mailcontact }}).

Plus d'information sur [la permanence](/actions/decouverte.html).
