---
layout: atelier
#what displays when the item is clicked:
type: [Événement | Atelier pratique | Atelier théorique]
title: Lancement du FabPart Lab
subtitle:
tags: [permanence, participation, découverte, numérique]
image: assets/img/event/weber-corridor.jpeg
alt: titre image
infos:
  date: 2021-02-08
  horaire: 14h-15h
  lieu: Batiment Weber · Salle M106
---


Le FabPart Lab se lance ! Rendez-vous en salle M106 ou en ligne pour une présentation de ses activités. Au programme :

- quelques mots sur la philosophie du projet par Marta Severo
- présentation des modalités d'action et d'accompagnement du FP Lab
- échange sur les besoins en terme de contribution et de participation
