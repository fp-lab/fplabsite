---
layout: atelier
#what displays when the item is clicked:
type: Atelier découverte
title: Permanence
subtitle: Conception de projet
tags: [permanence, participation, découverte, numérique]
image: /assets/img/illustr/conception-43.png
alt: Conception
infos:
  date: 2021-02-15
  horaire: 12h-14h
  lieu: online
  etat: archive
---


Le FabPart Lab propose chaque lundi une permanence de 12h à 14h [en ligne]({{ site.data.sitetext.urls.videoconf }}), en attendant que la situation sanitaire permette un accueil en présentiel.

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.

La séance du 15 février sera dédiée à la phase de conception de projet. Elle sera organisée sous forme d'échange autour de sa définition et des outils qui peuvent vous aider pendant cette phase.

Vous pouvez également demander un rendez-vous individuel (en présentiel ou en distanciel) pour discuter d'un projet, obtenir des conseils, ou évoquer [un accompagnement sprint](/actions/sprint.html). Écrivez-nous à [{{ site.data.sitetext.urls.mailcontact }}](mailto:{{ site.data.sitetext.urls.mailcontact }}).

Plus d'information sur [la permanence](/actions/decouverte.html).
