---
layout: atelier
#what displays when the item is clicked:
type: Journée d'étude
title: >-
  Valorisations et appropriations numériques des patrimoines et des mémoires
subtitle: >-
  Communautés et pratiques d’écritures
tags: []
image: /assets/img/event/nddl.png
alt: >-
  Archives départementales de Loire-Atlantique - 334 J - Photos: André Bocquel
infos:
  date: 2022-04-14
  horaire: 9h30-17h
  lieu: salle Séminaire 2 - Bâtiment Max Weber (U. Paris Nanterre)
  online: true
  etat: coming
---

La journée d’étude du 14 avril 2022 et l’ouvrage associé visent à explorer les pratiques collectives d’écriture numérique générées par les dynamiques d’appropriation et de valorisation de collections patrimoniales et d’objets mémoriels.

Afin de comprendre quelles formes de mémoires et de patrimoines émergent de ces pratiques, nous avons proposé d'explorer une perspective originale à partir de la notion de « communautés d’écriture ». Caractérisées par une pratique performative d’écriture et de réécriture, ces communautés se construisent autour d’une activité partagée de circulation et d’appropriation des objets et des ressources. Elles élaborent de ce fait des savoirs complexes, ancrés dans les pratiques et les territoires et à même de faire émerger et d’organiser une intelligence collective autour des patrimoines et des mémoires.

Voir [l'appel complet](/appel-je-valorisations.html).

Les contributions finales donneront lieu à la publication d’un ouvrage collectif dans la collection _Intelligences Numériques_ aux Presses Universitaires de Paris Nanterre, disponible en libre accès sur OpenEdition Books.

#### Relecture collective

Quatorze contributions ont été sélectionnées pour participer à la journée d'étude et co-écrire l'ouvrage collectif qui en résultera.
Depuis mi-mars, les textes soumis ont été mis en ligne et rendus accessibles aux contributeurs et contributrices pour une lecture collective à l'aide de l'outil d'annotation Hypothesis.
Cette lecture collégiale consiste à établir entre auteur·e·s une conversation critique et bienveillante.
Il s'agit d'enrichir et d'éclairer chaque texte de commentaires et de ressources, et de créer des passerelles thématiques et théoriques entre les contributions.

**La journée d'étude du 14 avril prochain rassemblera les contributeurs et les contributrices pour poursuivre et concrétiser les conversations initiées dans les marges des contributions.**

Ce protocole de relecture croisée s'inscrit dans une démarche expérimentale d'enrichissement et de validation par les pairs, qui trouvera son point d'orgue avec la rencontre des lecteur-auteur·e·s à l'Université Paris Nanterre le 14 avril 2022.
Les axes thématiques qui jalonneront nos échanges seront directement issus des échanges et des annotations récoltées pendant cette première phase de relecture.


#### Programme provisoire

- 9h30: Accueil des participant·e·s
- 10h00: Introduction et tour de table
- 11h: 1er axe thématique
- _12h30_: _pause déjeuner_
- 14h: 2e axe thématique
- 15h30: 3e axe thématique
- _17h_: _fin de la journée_

#### Avec les auteur·e·s

- Muriel Amar
- Sébastien Appiotti
- Jessica de Bideran
- Romain Bigay
- Françoise Cahen
- Nathalie Casemajor
- Grégoire Clemencin
- Fanny Delaunay
- Moulin Fabrice
- Julien Gaillard
- Jean-Pierre Girard
- Sylvie Grousset
- Jean-Robert Grousset
- Julien Hage  
- Agathe Le Riche - Maugis
- Marie-Hélène Lidec
- Margot Mellet
- François Millet
- Gilles Raveneau
- Clothilde Roullier  
- Caroline Rozenholc
- Claire Scopsi
- Shiming Shen
- Mathilde Verstraete
- Collectif MCI
