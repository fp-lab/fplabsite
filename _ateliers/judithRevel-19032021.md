---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Judith Revel
subtitle: Archives du commun ? Conservation patrimoniale et valorisation participative
tags: [participation, communs, musée, valorisation, archives]
image: http://www.unsam.edu.ar/lecturamundi/sitio/wp-content/uploads/2013/04/revel-4-624x415.jpg
alt: Judith Revel
infos:
  date: 2021-03-19
  horaire: 10h30-12h
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.b0099by1
  video: 10.34847/nkl.b0099by1/05fd2f07740ec72fa694b9b027a00d9887707292
notes: atelier-communs-minutes_20210319.md
---


#### Présentation
Pour ce premier atelier théorique, Judith Revel propose une critique politique de la notion de commun à partir du cadre de la conservation patrimoniale. Pour cette réflexion, elle s'appuie sur les échanges qui ont eu lieu au colloque [Archivos del común](https://www.museoreinasofia.es/fundacion-comunes/archivos-comun) en 2015, organisé par le [musée et centre d'art Reina Sofia](https://www.madrid.fr/musee-reina-sofia) de Madrid.

Judith Revel est [professeure au département de philosophie](https://dep-philo.parisnanterre.fr/navigation/les-enseignants/revel-judith-548268.kjsp) à l'Université Paris Nanterre et directrice du laboratoire [Sophiapol](https://sophiapol.parisnanterre.fr/).
