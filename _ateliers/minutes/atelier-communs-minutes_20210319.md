<!-- ---
title: FPLab - Atelier avec Judith Revel
subtitle: Archives du commun ? Conservation patrimoniale et valorisation participative
date: 2021-03-19
tags: fplab, commun, archives, atelier théorique,
--- -->

###### FPLab Atelier théorique - 19 mars 2021
# Atelier « Archives du commun ? Conservation patrimoniale et valorisation participative » avec Judith Revel

Qu'en est-il des archives dans d'autres disciplines de SHS ? "Disciplines anarchives"

Que se passe t il pour ces disciplines qui découvrent les archives ? ou qui en produisent à travers une documentation, une accumulation de traces ?

2eme chantier : travail sur la notion de commun, notion émergente dans la pensée philosophie politique contemporaine.

Redéfinition des notions de philosophie politique : notamment le «public», le «privé».

Mobilisation de références hétérogènes pour qualifier les communs.

Le commun, les communs, la chose commune (Res Publica),

- https://www.museoreinasofia.es/fundacion-comunes/archivos-comun
- https://www.museoreinasofia.es/actividades/archivos-comun-ii
- https://www.museoreinasofia.es/actividades/archivos-comun-iii

Intersection entre les archives et les communs.

Rejoint les questionnements du projet FP Lab : mémoires et patrimoines, valorisation, numérique.

Articulation possible entre archives et communs.

collage Leuciabia (?) avec un 3eme mot : "inapproprié"/inappropriable traduit inappropriate (en) &rarr; peut-être le lien entre archive et commun : la propriété ou l'absence de propriété.

## Commun

Ce qui ne devait pas recevoir d'images, ce qui n'était pas susceptible de recevoir de trace : devait être capable de disparaitre.

Ce qui est commun:  ce qui n'est pas reconnu comme objet de désir. présent en trop de lieux. trop plein d'existence : générateur d'invisibilité.

Dans les maisons bourgeoises: lieux de la domesticité (espace soustrait à la vue des visiteurs), le huis-clos de ceux qui faisaient fonctionner la maison mais ne devaient pas apparaître: les coulisses de la vie sociale, l'envers, mais aussi **les conditions de possibilités du fontionnement du social**.

voir _Les bonnes_ de Jean Genêt : invisibilisation et déni

Déni permis par une compression permanente de ce qui ne devait pas être vu.

Archives du commun: ce n'est jamais les archives "des communs" car elles sont "inappropriées", inadéquates.


La question de ce qui est inapproprié : ce qui est disparate. ce qui rend visible ce qu'on ne pourrait ou ne devrait pas voir.

inadéquation : ce qui est socialement inadmissible, ce qui ne devrait pas être vue dans un contexte donné.

Surgissement sous le regard, ce qui rend visible l'invisible (l'invisibilisé).

Comment l'invisible social est devenu visible. Souvent livré par des traces documentaires, par des archives particulières : écho différé de ce qu'on pensé effacé.

Variété des documents qui font trace. archives écrites, sonores. Point de vue de Foucault : textes qui ont été sauvés du silence et de l'effacement.

légende noire:
> « par nature sans tradition. [...]
> le hasard la porte dès le début »

Oubli réitéré: c'est sa condition, brisé par un événement, ressurgissement des fragments anonymes: c'est l'«éclat du pouvoir» (Foucault), comme un choc brutal avec le pouvoir qui produit une mise en lumière.
les archives des infâmes permettent de remettre en lumière des vies cachées par le pouvoir et retracées par le travail de Foucault.   

## Quelques exemples

- Journal intime: journal d'une femme de chambre
- mémoire de pierre rivière
- journal d'arqueline barbin
- Nuit des prolétaires (Rancière)

Inapproprié: ce qui surgit et qui ne devrait pas être vu.

Diff. entre fictionnel et réel

Rencontre simultanée d'un choc avec le pouvoir.

Commun comme avoir en commun qlq chose: coappartenance reconnu comme la sienne. Comme si le commun précédait toujours la communauté.

condition de possibilité absolu de toute appartenance communautaire ?

Communauté construit sur une ressemblance. Le partagé serait ce commun, dans lequel chacun se reconnait.

Or il y a là une tautologie. J. Revel cherche construire un contre-discours de cet tautologie.

On a pensé que le vivre ensemble, l'être ensemble était l'effet du commun, mais aussi sa cause.

Revel considère cela problématique: ce qui fonde la communauté est de l'ordre de l'invisible, et cet invisible renvoit à une transcendance : leurre, mystifications qui habille l'invisible.
comme par exemple les incarnations pĥysiques et matérielles du commun.

Ce que le commun devient, lorsqu'il tente d'échapper à l'invisible : drapeau, etc.

Commun invisible, qui nourrit des constructions qui sont des fantasmes d'identité et de ressemblance.

L'engouement sur le concept de commun participe du mouvement contraire.


Le mouvement contraire : redonner au commun une visibilité, réinventer une grammaire du politique : proposer un régime de visibilité sociale et politique.

effet: déconstruit les clivages qui structurent a pensée politique moderne.

notamment l'opposition/partage entre le privé et le public.

## 3 dimensions

### 1. bien commun

analyse du commun en terme de bien commun. à partir de la mise en danger de ressources naturelles, dès les années 60.

Hardin (1958, Science): tragédie des communs : face à une demande très forte, une ressource limitée, si elle demeure en libre accès, alors la ressource est vouée à l'épuisement.

Soit on enclos cette ressource, soit on la laisse en commun.

Assimile la présevation des communs au bien commun. Négation des interets particuliers pour l'interet commun.

ce qui structure son raisonnement: opposition d'intếret d'un seul avec l'intérêt commun.
il ne faut pas que la ressource ne soit pas appropriable : il faut dénir l'accès pour garantir l'accès. paradoxe.

parallèle avec l'archive: épuisement de la ressource ? exclusivité d'une ressource participe à sa valorisation. (évidence questionnable quand meme...)

on peut penser ces archives dans cette opposition public/privé, mais la polarisation finit par être inutile (partenariat public-privé). Archives sont devenues un marché.

Les institutions doivent se positionner sur ce marché.

ex: archives des philosophes, 4M€ pour les archives de Foucault par la BNF en 2013. Derrida achetées par Princeton.

Faut il encore parler en terme de privé/public ?



### 2. mode de gestion

on structure désormais le commun en le décalant par rapport au régime de propriété

ce n'est plus qui est propriétaire, mais comment on administre, comment on le gère (gouvernance).

Elinor Ostrom, dont le texte a fait débat.

le renouvellement de la réflexion sur le commun a pris son essor à partir du moment où on s'est intéressé aux formes d'administration et de gouvernance.

L'espace politique et juridique nous impose de demander.
un bien est techniquement un objet appropriable. Plus exactement, ca l'est devenu..

Puisqu'en droit romain, le commun a deux définitions:
- par nature innapropriable : res communis
- ce qui n'appartient encore à personne : res nulius


peut on penser le commun sans la propriété ?
ou alors: comme ce qui ne peut pas être approprié, mais dont on peut se servir.

On ouvre la possiblité politique et sociale d'une construction...

régime non-propriétaire de la participation et de l'inclusion sociale.
La participation ne s'inscrit plus dans un régime propriétaire.

Depuis tous ces lieux de conservation, d'archives, etc., il faut se demander ce qu'il en serait d'une redéfinition non propriétaire des pièces d'archives et fond documentaires, conservés au nom d'un commun que nous avons en partage, qui se constitue en fonction de ses usages et non de ses identités.

penser l'usage et non le titre propriétaire des archives:  archives comme nouvel articulateur au sein des politiques de conservation.


Tension entre inappropriable et inapproprié (inadéquat, choquant, hors-lieux, déplacé..)

déplacé: vulgaire, obscène, etc.
inappropriable: ne peut donner lieu à un titre de propriété.

ce qui est adminissible: la critique de la propriété, de la logique de l'appartenance.

revendication par l'équipe du musée, volonté de constituer des archives inappropriées:
terrain des usages ouverts, accès inconditionnés à ces ressources.

coresponsabilité et coopération pour ces matériaux documentaires

### 3. la fabrique du commun

Considérer le commun, non pas comme qlq chose qu'on **a**, ou **utilise** en commun mais comme qlq chose qu'on **fabrique** en commun

la fabrique hors du champ de la propriété.

Evolution du travail : résistances

propriété intellectuelle

mise à disposition de tous et de toutes d'un travail qui passe par une fabrique partagée.

Parallélisme avec les archives.
les archives ne le sont pas par nature. On les fabrique, gestes techniques précis, qui sont de l'ordre de la production.
Ce que nous produisons, c'est le produit d'une chaine qui fait partie d'une politique des archives, d'une construction/invention des archives.

produits de la coopération humaine, à partir de leur matérialité.

Si elles sont coopératives, alors ... ?

archives comme le produit d'une activité coopérative.

---

Exposition sur l'activité de Mathilde sérigue et Carole Seropoulos (usage de la vidéo dans une perspective militante): fabrique d'archives, fabrique d'expériences partagées et création d'un centre Simone de Beauvoir. Il s'agissait de construire du commun.
(Delphine Seyrig et Carole Roussopoulous)
merci!
remarque: dimension du "digital" et du numérique. ;)

2 questions:

- visite Musée Pauline (?) il ne reste rien des juifs polonais, toutes les traces ont été effacées. Or ce musée fait un usage du numérique pour donner à voir. Reconstitution. prise d'assaut du vide par le numérique pour le recouvrir, or il fallait au contraire documenter le vide. Logique d'artificialisation du réel.
- Shoram Makaremi (?) projet Offsite, consacré à la violence et à la politique de la mémoire en IRan (post-révolutionnaire). Séminaire sur une série de photographies de manifestations de femmes en mars 79. Les films ont été conservées, déposées au centre Simone de Beauvoir.

2 vides recouverts:

- de l'ordre de l'artifice
- enregistrements

est-ce que le numérique n'est pas ce qui risque d'être trop facilement la tentation du remplissement du vide ? Le vide est une trace comme une autre.


### Marta S.

ex du dépot légal du web à la BNF.

archives du web comme un exemple d'une archive qui sort de la propriété.

le "dépot" à la BNF semble recréer une forme de propriété.

Louise Merzeau: réflexion sur la mémoire partagée.

Liaisons possibles avec les enjeux de la restauration d'oeuvre : fragments restaurés visibles, ou invisibles aux spectateurs ? A l'identique, ou en incrémentation de la modernité ?

En tous les cas enjeux de la dématialisation : matérialiser avec/par le vide en contexte de plateformisation ?

Que fait-on de la question de la matérialité des gestes de consultation ?

### Nicolas S.

Quelques réponses en philosophie du numérique. Mimesis et formes de représentation différentes.
Sans doute un échec du Musée Pauline, mais peut-être aussi un oubli, celui du traitement du vide.
La question de la matérialité se superpose aux changements de supports qui permettent d'interroger les ontologies et de requestionner le sens : chaque média vient questionner et remédier les précédents, ils se sont simplement ajoutés en apportant notamment de nouveaux champs de recherche.

Le tryptique bien commun - gouvernance - fabrique semble intégré aujourd'hui dans la pratique des communs.


### Ghislaine G.
Perspective critique autour des usages.

### Judith R.
Comment constituer des communs à partir des mémoires ?

Magali Bessone sur la réparation (mémoires de l'esclave). pas de réparation si on remplace une histoire par une autre histoire. mais nécessité d'un horizon de la réparation. On ne rattrapera pas le temps perdu, mais on doit faire de la place à diff. mémoires : cohabitation nécessaire des mémoires divergentes pour pouvoir donner la parole.
Economie restrictive de la parole : moins elle est donnée, et plus sa prise est approximative.

### Christophe Tuffery

En archéologie: le numérique produit des traces, y compris dans les normes qui sont mises en place.

notion d'archives du sol (Leroy Gourhan), traduites dans des archives de fouilles (carnet de terrain, etc.) au statut public (document à caractère administratif).

Est ce que l'open access participe d'un "commun patrimonial" ?

- J.Revel
En ethnologie, les carnets de terrains faisaient l'objet d'une transmission et non d'un dépôt : créant des filiations scientifiques.

### Nicolas S.

Le patrimoine n'est-il pas déjà un commun ? Open-acces ?
Il semble que les conditions n'en soient pas encore réunies.

### Judith R.
Traces et usages des archives doivent être mis ensemble (type de relations, d'ouvertures, de gouvernance, ... : productions communes qui viennent se surajouter)


### remarque: Alessia Smaniotto
Dans les questions d'accès ouvert, archives ouverte, etc le "commun" est moins la contribution X ou Z déposée, que l'infrastructure qui rend le partage possible
