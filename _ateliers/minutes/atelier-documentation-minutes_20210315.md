<!-- ---
title: FPLab - Atelier des chercheurs 15 mars 2021
date: 2021-03-15
keywords: fplab, atelier, documentation
--- -->

###### FPLAB - Atelier pratique - 15 mars 2021

# Outils et méthodes pour la documentation d'un terrain de recherche, avec Sarah Garçin

> L'Atelier des chercheurs développe [des outils et des méthodes](https://latelier-des-chercheurs.fr/outils) pour la documentation collaborative d'activités pratiques. Pauline Gourlet nous présentera les principes
 et la philosophie de ces outils et nous verrons comment ces outils peuvent être utilisés sur le terrain par les chercheur·e·s pour documenter leurs travaux.
>
> Sarah Garcin est designer, artiste et enseignante, elle a fondé le collectif [l'Atelier des chercheurs](https://latelier-des-chercheurs.fr/) en 2013 avec Pauline Gourlet et Louis Eveillard.

## Collectif L'Atelier des chercheurs

- plusieurs casquettes : designers, chercheur (Pauline)

> "transformer les manières d'apprendre et de travailler"

outils libres de droit, open-source, communauté de contributeur (fablab)

## Enseigner le design à l'école primaire

mobilier, typographie, électricité, réactions en chaine, tampon, etc..

constat: activités personnelles et oubli d'une séance à l'autre

dispositif dans la classe:
    - changement de pratique dans la classe
    - documentation (via banc titre) des étapes importantes de leur production : photographie

Constitution d'une bibliothèque d'image pour documenter les diff. projets de la classe.

Introduction d'un nouvel élément dans la classe, provoquant une queue pour documenter, et finalement un espace et un temps prétexte à la discussion.

Principes reproductibles pour d'autres activités en classe

→ Hypothèse: documentation en classe selon 4 phases:
1. production de médias
2. édition, sélection
3. organisation dans un récit (ou publication), blog, montage vidéo, montage son, animation, print
4. partage du récit/production

Cette expérimence a mené à la conception de **DOdocs** : développement in-situ, avec les acteurs du terrain, en itération

Quelques principes :
- Dimension tangible : station de documentation, reproductive en fablab
- Collaboratif temps-réel
- API qui permet d'en faire de novueau usages > publication

Voir l'instance en ligne : [test.dodoc.fr](https://test.dodoc.fr)


Observation: un tel dispositif de documentation génère de nouvelles activités/initiatives, et donc une nouvelle dynamique.

pertinence dans un contexte de participation (terrain <-> recherche)

itérations selon les contextes : pour chaque atelier, nous avons développé une fonctionnalité (ex: la publi page à page)


- cahiers du studio : timeline-centered
- corpora: créer des corpus de recherche (organisation en dossiers/fragments)
- Plateau: documentation de workshop, basé sur des pads, organisation par thématiques (durée/compte  rebours)
- prise de note collaboratif + images
    - blocs timecodés, liées à l'image/webcam/son
-

## Technique

- node electron vue.js
- serveur VPS
- pas de base de données : _flat file_
    - fichiers yaml
    - media+yaml
    - idem pour les publications

## Questions

- export structuré ?
    - API documenté, mais non diffusée
- catégories: aller vers des métadonnées documentaires
- modularité : construction d'autres logiciels
    - node electron vue.js
    - serveur VPS
- usage mobile: fonctionne bien
- dispositif centré ressources
- geolocalisation : chantier possible, voir aussi l'outil mille-feuille (prototype)

Soutien:
- https://forum.latelier-des-chercheurs.fr/
- https://forum.latelier-des-chercheurs.fr/t/installer-do-doc-sur-un-serveur-dedie/210
