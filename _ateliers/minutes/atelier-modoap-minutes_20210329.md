<!-- ---
title: FPLab Atelier «Machine et deep learning appliqués aux SHS»
date: 2021-03-29
keywords: fplab, atelier, machine learning, deep learning,
--- -->

###### FPLab Atelier 29 mars 2021  
# Machine et deep learning appliqués aux SHS, avec Julien Schuh et Cyril Bruneau

MODOAP Modèles et outils d'apprentissage profond - modoap.huma-num.fr/

production d'outils réutilisables pour l'exploration de corpus

2 constats:

- "masse" de données - difficulté d'analyse et d'appréhension
- nouveaux outils d'analyse de textes et d'images faisant appel à du ML/DL

Projets existants en SHS ne mettent pas à disposition des outils. L'objectif n'est pas d'inventer de nouveaux outils, mais de le democratiser en les insérant dans une chaine complète de traitement

Production d'outils pour un projet, outils souvent peu réutilisable (nécessite un fort investissement avec ingé/Développeur)

principe: proposer une boite à outils en ML/DL

1. Outils pour interroger des bases de type Gallica
2. Outils qui peuvent réaliser certaines tâches et qui peuvent être repetées sur d'autres corpus (ex. decter les illustrations dans des documents)
3. Mise en valeur des données traitées en proposant des visualisation et des modèles

L'objecti est de permettre la comparaison des corpus

Les modèles et les chaines de traitements sont conservés et réutilisables,

Plusieurs fonds (sous-projets, fonds non documentés et opaques)
- fond Elie Kagan (La Contemporaine)
- corpus de manuels scolaires (BNF)
- presses

Projet low tech : serveurs jupyter melant code et texte

## Outils

### Détection d'illustrations

détecter et segmenter les illustrations dans des pages de mag/journaux
https://modoap.huma-num.fr/outils-realises/

Entrainement suppose une phase d'annotation : annotation manuelle

fichier d'entrainement (500 exemples) issu du zoning effectué/fourni par Gallica.

Plusieurs modèles sont nécessaires (temps d'entrainement très long)

l'entrainement a lieu sur des formats image (jpg)

### Questions

- Cédric: Vous avez quel taille pour le jeu de données d´entraînement ? et quel jeu de données
- Carmen Brando serait il possible d'avoir de précisions sur des pré-traitements éventuels à faire sur les images afin que tout fonctionne correctement ?
    - transformation pdf > jpg
- Denis Teyssou Bonjour, est-ce que vous conservez un pointeur (un lien ou un numéro de page où figure l'illustration extraite ?
    - on conserve le contexte avec indexation de pixels

### Classification automatique d'images

Possibilité d'appliquer des étiquettes aux images : binaire, multi-étiquette
Chaque image peut avoir plusieurs étiquettes

détection de doublons VS détection de similarités suppose des modèles différents.

Modoap ne recrée pas les architectures, elles testent et adaptent des modèles existants.



- Denis Teyssou Est-ce que sur ce corpus de photos (de manif), envisagez-vous d'extraire les panneaux, banderoles et d'en "OCRiser" le texte ?
- Cédric avez vous pensé à utiliser un algorithme de clustering en non supervisé, par exemple sur votre jeu de données de photographies ? ca pourrait donner des résultats intéressants
    - à suivre
- Carmen Brando Pour les entrainements, la question de GPU se pose, c'est donc côté client qui tout se passe si je comprends bien..  est-ce que cela ne sera trop lourd en temps de calcul ?
    - utilisation de l'infrastructure Colab pour le moment.
-  Michel Bernard Est-il possible de récupérer les critères utilisés par l'algorithme ?
    -  Cédric: a priori on peut toujours récupérer les parametres des modèles, mais pour les interpréter il faut des techniques spécifiques (par exemple de la réduction de dimensionnalité)

- Romain M. Il y a possibilité d'accéder à du temps de calcul sur des grosses machines type Jean Zay, même pour les SHS
    - L'idée de ces carnets Jupyter serait qu'un·e chercheur·r isolé·e puisse tester/expérimenter des modèles sur ses corpus sans avoir à mobiliser des institutions/partenariats nécessaires pour accéder à des infra de calcul

- Claire Clivaz D'après vous, quelles seraients les questions de recherche les plus pertinentes à poser à ce type de corpus? Que va-t-on y trouver par ce moyen qu'on ne trouverait pas autrement? Des études test ont-elles été faites dans ce sens?


### Calcul de similarité entre images

### Détection de doublons


Questions de recherche:
- peut on repérer de manière massive les phénomènes de circulation, de diffusion
- histoire sur le long terme de ces circulations

les outils permettent de montrer la systématisation de la diffusion d'objets graphiques en Europe entre revues


PixPlot (du DH Lab Yale https://dhlab.yale.edu/projects/pixplot/)
url pour Kagan ?


### Classification automatique de textes

- simpletransformer (https://simpletransformers.ai/docs/classification-models/): associer des étiquettes aux textes
- https://spacy.io/

choix d'annoter sur un texte segmenté au niveau du paragraphe

Les objectifs sont :
- identifier les entités nommées (lieux)
- modélisation avec topic modeling

### Détection d'entités nommées
### Modélisation thématique

Types d'analyse avec detection textuelle :

- identification des reprises de textes et des phénomènes de ciruclation (ngram)
- remédiation et reappropriation massive des textes (ex. manuels)
- possibilité d'analyser des textes en plusieurs langues
- identifier les ecosystèmes médiatiques qui se construisent autour de manuels ou guides (plagiat ou logiques industrielles de réemploi)

## Questions

Marta S.: quid des corpus nativement numériques (Wikipédia, réseaux sociaux ?)
    - on reste sur des dynamiques de réappropriation. les corpus anciens sont également des objets culturels pris dans ces dynamiques de circulation

Claire Clivaz : Je m'intéresse toujours aux cas limite: avez-vous repéré une anaphore qui n'a pas été listée? Et si oui, est-il possible d'entraîner la machine pour les cas limites, ambigus, etc. ?


Cédric : comment vous envisagez d´intégrer des approches de data science à des questionnements historiographiques ou épistémologique plus traditionnels (par exemple autour de l´etude du document historique)? est ce que c´est une question que vous vous posez pendant le projet


L'objectif est également d'inverser la manière de réaliser ce type d'analyse : partir de l'objet et non de l'interet statistique

Ne pas partir de la statistique pour produire des grilles d'interprétation, pourquoi ne pas modéliser des formes d'interprétations, hypotheses sur les objets pertinents selon les formes d'interprétation
