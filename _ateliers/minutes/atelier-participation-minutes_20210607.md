<!-- ---
title: Atelier FPLAB - Cyril Isnart - La participation en pratique. Une généalogie intellectuelle et deux expériences
date: 2021-06-07
tags: fplab, participation
--- -->

###### FPLAB - Atelier théorique - 7 juin 2021

# La participation en pratique. Une généalogie intellectuelle et deux expériences, avec Cyril Isnart

Comment la participation a changé les objets de recherche :

- rapport au religieux
- production de la musique traditionnelle européenne

## 1ere phase
EN 2005: travail de terrain et en archive.
Vallée de la Roya, déjà un lieu de passage et de migration

Sur les traces de Bernard L. Jacob (ethno-musicologie à Nanterre).
Ce qu'il reste sur le terrain des pratiques décrites par BJacob

2006: sauvegarde du patrimoine immatériel : participation et inclusion des porteurs de patrimoine dans le processus de conservation et classification des patrimoines immateriels: principe du bottom-um

2005: convention de Fargo: mise au centre de la société qui doit valider la conservation du patrimoine.

Séminaire centré sur le patrimoine immatériel et sa mise en oeuvre.

MCC: fin de la mission du patrimoine éthnologique: système de favorisation des recherches ethno au sein du MCC qui se transforme en instance de mise en oeuvre de la convention sur le patrimoine immatériel.

Travail avec Chiara sur le terrain de la vallée de Roya (?) pour la production de fiches
Mise en place de la participation avec les acteurs locaux.

Blog PCI ICH: travaille sur ces questions de participation, de patrimoine immatériel,

La participation existe déjà dans diff. couches épistémologique:
- réflexions sur l'anthropologie du patrimoine (avec Daniel Fabre), même si c'est autour des monuments historiques
- mouvement des écomusées, muséologie sociale: ce sont les habitants qui doivent être actrice de leurs musées et de leurs expositions. (Hugues de Varine)
- Critique de l'anthropologie comme science de la domination. La parole et le statut de l'anthropologue sont remis en cause. Ce n'est pas nécessairement une parole autorisée, et manière de se rattraper sur l'image colonialiste de la discipline. Solution: développement de la co-écriture: intégrer la parole, la voie et l'agentivité des personnes étudiées pour renouveler la manière même de faire de l'anthropologie.

Héritage dans les années 2000 pour se saisir de la participation.

## 2eme phase
départ au Portugal pour 5 ans avec un projet de valorisation d'un chant polyphonique (festival, érudits, etc.)

Mais résistance très forte à l'idée de rentrer dans le patrimoine immatériel. Raison:
- démocratie qui n'a que 35 ans: méfiance de l'institutionnalisation des pratiques, notamment parce que cette pratique est une pratique contestataire (de gauche).

par contre les associations locales et nationales vont s'investir fortement dans ce patrimoine immatériel.

Interprétation de la convention: Ceux qui doivent porter les projets de classements au PI, ce sont les collectivités territoriales.

Le chant est finalement inscrit en 2014 au patrimoine de l'unesco.


### Apport théorique de cette deuxieme phase

la participation, en tant qu'elle est fabriquée par l'institution culturelle, est une alternative à l'ethnographie scientifique.
Va de pair avec l'anthropologie contemporaine: entre l'action culturelle et l'anthropologie

la participation est vue comme une nouvelle chance (?): nouvelle fonction sociale du monde culturel. Les institutions vont trouver un novueau sens et une nouvelle fonction dans la société:
- pour lutter contre une forme d'élitisme du monde culturel,
- mais aussi sorte de revanche du monde culturel qui est touché par les restrictions budgétaires. La participation arrive comme une justification et une opportunité pour conserver une place dans la société.

## Phase 3: collaboration avec le Mucem

impliqué dans la vie scientifique du Mucem, recherche et enseignement.
Actualité curatoriale à son arrivée: fabrication de la participation (?) avec 2 expos:
1. Mémoire des luttes contre le VIH Sida: mise en scène de la lutte et de la mise en mémoire.
    - Le musée met en place un groupe de 40 personnes de la société civile liées à la lutte contre le sida qui forme un comité scientifique pour discuter tous les choix qui feront l'exposition. Enjeux du collectif et du lien avec l'institution culturelle. Création de consensus autour de l'exposition.
2. Les savoirs-faire des populations des gens du voyage en europe et méditerrannée: comité composé d'une quinzaine personnes des communautés (resp association, resp. lieux cultures, resp. de dispositifs sociaux, chercheurs spécialistes) : fabrication du consensus autour de cette question

Séparation de ces deux projets avec le reste du musée.

Proposition au sein du musée: fabriuer un lieu afin que les personnes concernées par ces initiatives puissent travailler, réfléchir, mettre à plat la question de la participation:

- Inviter des gens qui sont engagés dans une pratique participative
- Proposition d'un programme de 6 rencontres pour réfléchir de manière thématique sur la participation:
    - qu'est ce qu'un département des publics entend par participation: quelles sont les pratiques au sein du musée
    - les collectes contributives: dispositif pour enrichir les collections (Christine B et Sylvia Amar - les nouveaux commanditaires), comment créer une collection par le bas
    - la documentation collaborative des collections, alternative à celle de l'équipe conservation (voir Herbonautes, projet Transcrire)
    - contrats, rémunérations et contreparties de la collaboration: où s'arrêtent la participation et où commence le travail de l'expert. Comment le musée peut donner une contrepartie à la contribution/participation: par exeple reconnaissance des compétences lorsque les gens participent et collaborent.
    - gestion des projets d'expo. participatives: comment on gère une telle exposition (cité de l'immigration), musée de rennes
    - journée d'étude avec deux intervenants: Schluss (MHN) sur les sciences citoyennes, et Joelle Zask (Université Aix-Marseille)

Penser à une application pratique de ces rencontres


Fabrique de fiches pratiques reprenant les éléments de chaque atelier, et ces fiches sont allées rejoindre le vademecum du musée: sorte de mode d'emploi pour la collecte sur le terrain pour enrichir les collections. Le vademecum a donc pris une couleur plus participative qu'auparavant, alors qu'il héritaient des pratiques de collection des arts populaires (plus top-down).

4 questions qui sont ressorties des ateliers :
- limites entre travail et participation (donner sa part). Quel usage le musée peut faire d'une participation ?
- instrumentalisation: qui instrumentalise qui : le participant qui bénéficie d'une visibilité ou le musée qui bénéficie d'une expertise ?
- hiérarchie et légitimité: qui choisit quoi et qui a le dernier mot dans un contexte de montage participative d'exposition ?
- qui parle ? quelle parole est mise en scene dans la culture participative ? il y a certes un consensus, mais a t on réellement une seule voix ? et a t on une multi-vocalité exhaustive ? les processus de choix et la dimension de pouvoir font que toutes les voix ne s'expriment pas de manière égale.

Une certaine distance entre l'idéal de participation et la pratique.

L'atelier va se transformer, avec de nouvelles journées d'étude.

Appel à collection contributive sur les objets du confinement: 600 propositions d'objets du confinement;

### Second projet participatif: Séminaire «Singulier, les objets des minorités en europe et méditérannées».

repose ces mêmes questions sur les objets des minorités autour desqueles se construisent les minorités.

Travail comparatif d'un pays à l'autre.

Réflexion sur Matérialité: comment un objet est vecteur d'identité ?

Démarche participative: association chercheur + membre minorité: choix et présentation d'un objet qui fait sens pour une communauté.

quelques questions:
- qu'est ce qu'une minorité?
- statut fluide (et fuyant) des participants aux duo: scientifiques, conservateurs, minorité.
- prédominance de ceux qui savent déjà parler. maitrise des codes et des enjeux de la prise de parole. Réflexivité dans le discours de l'expérience minoritaire : c'est davantage un partage du champs, donc est ce qu'on est encore dans la participation ?
- Refus des participants: certains chercheurs ne veulent pas être instrumentalisés par leur terrain. Réification du statut du chercheur. De l'autre côté, certains minoritaires n'ont pas besoin des chercheurs et des institutions. Séparation de fait des pratiques.

Enjeux de pouvoir, de réification, de nomination.

## Conclusion

la participation:
- c'est du politique (_policy_) :comment on envisage le gouvernement et la gouvernance de nos sociétés contemporaines: enjeux de légitimation, hiérarchie, etc. Création des normes
- c'est aussi de la politique (_politics_): c'est un outil pour être dans la mêlée

En tant que chercheur, on se sent obligé d'y rentrer. place des SHS dans les sociétés, a t on le droit de partager nos résultats avec la société, peut-elle critiquer nos résultats.

Il faut participer à la participation, mais c'est très inconfortable: effervescence et difficulté de tous se parler. Mais c'est la seule solution pour comprendre la société. Faire des choses pour faire avancer la compréhension de la société.

## Questions

### Marta S.:
- dans la pratique, comment on repositionne l'expert (dans l'atelier ou autre)
- peut-on construire une base de connaissance

Sur l'expertise (Tornator): comment faire de la démocratie patrimoniale: dialogue, débat et discussion et mise en commun des arguments: suppose la délibération commune, consensus accepté par tous.
préalable: pas d'expertise supérieure à une autre. Donc dans une telle discussion de projet participatif, il faut faire le deuil d'un expert qui sait. Parmis d'autres experts qui ont des savoirs particuliers qui valent autant.
mais limite de cette position, c'est que le consensus qui est construit est plus proche du plus petit dénominateur commun que de l'objet final souhaité.

exemple: on risque d'arriver à des résultats qui tombent sous le sens: banalité du résultat consensuel.
l'expertise à plat.

L'ouverture permet que chacun s'exprime, mais le résultat ne sera pas à la hauteur du système mis en place.
Les moyens justifient une fin pas si intéressante que ça.

autre approche:
Si on conserve une figure d'expertise, alors les règles de aprticipations ont leur limite, puis à chacun de vouloir participer ou non.

### Valérie Perlès
Toutes les questions dans la cadre muséal se retrouvent dans le PCI.

Hors dans le cadres des écomuses, on avait résolu la question, avec des terrains d'ajustement, mais autour du PCI, on est obligé de tout refaire.

Négociation avec la position d'expert.

le chercheur accompagne la communauté à exprimer sa parole: rend la participation possible, pour éviter la confiscation par des érudit locaux, par des associations/lobbys

chercheur-garde-fou pour créer un espace de réflexivité dans un mécanisme de patrimonialisation

Question du détournement de la patrimonalisation à des fins politiques ou économiques.

exemple des fiches du village: un érudit voulait prendre le pas sur la description du chant.


### Nicolas S. exemple vertueux ?

pas de solution miracle qui marche à tous les coups: tient compte du contexte, de l'historicité d'un objet

on est tous dans l'idéologie du projet (Julie Léonard sur la patrimonalisation du fesnoz), mais reconfiguration permanente du projet, la patrimonalisation doit s'adapter à la vie d'un objet culturel.

Nouvelles institutions de la culture populaires suite au PCI. Inventaire nationales + institutions nationales pour donner les instruments de patrimonalisation. Les communautés s'établissent aussi avec ces institutions expertes de la patrimonalisation

Fabrique 'industrielle' de la participation

### Marta S. - peut-on créer un modèle de participation que l'on peut reproduire ?

Pour les herbonautes: modèle de participation faisant appel à des amateurs botannistes via un community management: recrutement, motivation, récompenses, mais aussi validation par un conservateur: on n'est pas dans la démocratie patrimoniale, mais dans une participation à un projet muséal.

nécessité d'énoncer les règles clairement.


### Ouverture de la gouvernance des projets

L'ouverture de la gouvernance a été une des conditions pour que le projet collectif fonctionne.
