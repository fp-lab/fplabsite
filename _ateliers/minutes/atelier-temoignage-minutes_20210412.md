<!-- ---
title: FPLab - Atelier «Documentation et valorisation des témoignages»
date: 2021-04-12
keywords: fplab, atelier, archive orale, témoignagne, entretien, documentation
--- -->

###### FPLab Atelier théorique - 12 avril 2021
# Documentation et valorisation des témoignages, avec Claire Scopsi

Méthodologies de collecte d'entretiens de mémoire

Approche SIC empruntée à l'anthropologie (?)

Observer comment les gens faisaient des entretiens.

travail sur l'identité des communautés immigrées. Comment faire mémoire. Identité collective à partir des mémoires

Place privilégiée des témoignages oraux dans ces mémoires

S'appuie sur les travaux de FLorence Descamps
Travail avec des archivistes oralistes.

Différentes méthodologies de collecte des entretiens oraux. Préoccupation principale : il-y-a, avec des dispositifs similaires, des manières différentes de collecter des entretiens.

Puis, mise en pratique.

## Typologies d'inscription de la parole

Pourquoi on enregistre ? Retour historique emprunté à Florence Descamps

Enregistrer un témoin est un acte scientifique banal, pratiqué dans bcp de disciplines, notamment anthropo, socio, éthnométhdologie, ethnomusicologie.

Recueil de données, mais l'acte ne va pas de soi.

L'arrivée de la vidéo numérique (2005-2006) a complexifié les choses (paradoxalement..)

Organisation de premiers ateliers : beaucoup de participants se plaignaient de témoins qui étaient mécontents de leurs enregistrements, voire interdisaient leur réutilisation. Plus sensible chez les documentaristes, moins chez les archivistes qui ont sans doute développé des méthodes et des protections.

Un témoin mécontent est un témoin qui a été martyrisé. Comment éviter ces situations ?

Partir d'un ensemble, d'un corpus avec la situation suivante :
- une personne qui parle de son expérience (un témoin),
- une personne qui l'écoute, le relance, le questionne, souvent à l'origine de l'entretien (le témoignaire - qui reçoit le témoignage),
- et un appareil d'enregistrement (manuscrit, voix, audio-visuel, etc.)

Catégorisation en fonction du contenu (thématique du témoignage). Mais en fait, ce n'est pas tant le contenu et la discipline qui caractérise la pratique, que de l'objectif du témoignaire et du statut du témoin. Et objectif de valorisation et de documentation.

Distinction sur les formes plutôt que sur les contenus.

### Typologie des logiques d'inscription de la parole.

Au commencement, l'archive orale. Enregistrer ceux qui n'écriront pas, remplacer la mise en mémoire par écrit par l'enregistrement est une tradition ancienne.

Fin XIXe: enregistrement de témoins aux US en manuscrit
Archive orale: tradition du début du XXe siècle
Ferdinand Brunot enregistre entre 1911 et 1914 des chansons, des dialectes, des contes, dans le cadre des «études folkloristes».
Cylindres numérisés par la BnF et écoutables aujourd'hui.
Enregistrement également de parlés africains en enregistrant deux jeunes hommes angolais émigrés en France en 1914. Premier exemple en France de collecte de témoignage d'immigré.

Ces pratiques questionnent **le statut des témoins**.
Deux témoins dont on ne donne pas le nom. Qui sont là pour ce qu'ils représentent. Certains personnes, encore aujopurd'hui considèrent qu'il n'est pas utile de faire signer des droits de diffusion à leurs témoins que ce soit dans le cadre d'enregistrements ou de prises de vue.

Dans les années 30, évolutions technologiques. Fondation d'un institut à la Columbia Universiy

méthodes journalistiques pour interviewer des grand personnages : enregistrements d'archives sonores de témoignages de grands personnages.

En France l'histoire orale s'est pratiquée autour d'enregistrements des grands corps d'état (hommes politiques, personnels ministériels, banques, préfectures), conservés aux archives nationales, à la BNF.

«Archives orales par le haut» F. Descamps.


### Ecole de Chicago
entame un travail sur les communautés immigrées &rarr; histoire sociale par le bas : femmes, ouvriers, immigré·e·s, etc.

Histoire des mentalités, micro-histoire, histoire populaire des courants de pensées, histoire du travail, etc.

On justifie l'utilisation de l'enregistrement oral par le fait que ces populations écrivent peu.

- Anthropo et socio : "par le bas"
- Socio: histoire "par le haut"

### Public History (Raphael Samuel) en Angleterre dans les années 60.
Sorte d'université populaire, cours du soir pour les travailleurs pour se former et suivre des études théoriques.
Les étudiant·e·s étaient amenés à faire des travaux de recherche dès la première année : sur leur environnement social, leur usine, etc.

Initie une histoire du féminisme, histoire des quartiers, etc.

Initie ainsi ce que seront les différentes _studies_

### Enregistrement des procès nazis
Ouvre le questionnement en Histoire: est ce qu'on peut faire de l'histoire à partir d'enregistrement de témoins ? Que faire de l'émotion des témoins ?

en France, on va défendre de ne pas utiliser l'enregistrement des témoins, notamment avec Paul Ricoeur: Une archive doit être écrite, la parole doit être séparée de son auteur.

Ailleurs, en Italie, en Allemagne, on va au contraire utiliser l'archive orale pour comprendre l'adhésion des populations aux idéologies fascistes.

Utilisation scientifique d'archives orales, conservées sur le long terme.
C'est la destination et l'objectif de valorisation qui distingue une archive scientifique.

Les archives orales sont constituées par des archivistes avec l'intention de les conserver advitam. Conservation à destination des générations futures. Il s'agit de restituer précisemment les contextes qui entourent l'enregistrement.

## Récit de vie

Alex lainé (Faire de sa vie une histoire)

Pratique clinique d'enregistrement de témoin, à visée thérapeutique. Le témoignaire est psychologue et il fait parler un _patient_, qui se trouve dans une situation critique.

Le récit de vie conduit cette personne à faire un bilan.

Entretien (témoignaire psychologue) et/ou rédaction par le témoin d'un ouvrage/monographie. En exprimant son parcours, il s'agit de le remettre en perspective et de prendre du recul sur le présent.

Problématique: le récit de vie devenait la recherche.
Bourdieux s'y oppose pour l'illusion biographique du récit de vie.
Régine Robin également :
la chronologie donne l'impression que toute la vie d'une personne est conditionnée par son origine et son parcours. Sorte de déterminisme de l'origine.

Forme de genre: clichés de vie

Au contraire, d'autres chercheurs vont l'utiliser, Daniel Berteaux par exemple, pour la mise en évidence des lignées et des transmissions familiales.

## Journalisme

On retrouve le même dispositif de témoin/témoignaire/dispositif d'enregistrement.

mais la temporalité est particulière: tout va très vite, objectif d'efficacité.

un journaliste sait toujours ce qu'il veut faire dire au témoin. Le journaliste cherche à illustrer une idée/une information.

Diffère d'une archive orale, où le témoin n'a pas le même statut, où le témoignaire n'a pas une idée préconçue de ce qu'il veut entendre/recueillir.

## Juges

doit faire émerger des témoignages
développement du récit de témoins à partir des années 80, notamment à partir des procès filmés et du film Shoah.

Aboutit à une libération de la parole des déportés et à bcp de projets d'enregistrements de témoins.

54K vidéos via la Shoah Fundation de Spielberg
ou encore le mémorial de la Shoah en France

## Trois modèles: Preuve (vérité) / Medium (communication) / Index (trace)

Trois Modèles dans lesquels on peut se positionner très simple

**Preuve-source** : enregistrement pour faire de l'histoire ou de la science: apporter une preuve pour en faire une théorie
  - dans ce cas, le témoin a un certain statut, avec un paradigme de recherche de la vérité.
  - suppose alors des méthodes particulières pour désambiguer, de vérification des affirmations
- s'applique aux historiens aux scientifiques


**Medium**: paradigme de la communication
quel est l'effet qu'obtient le témoin en parlant sur les gens qui vont écouter le témoignage:
est ce que le témoignage est fait pour transformer le monde ?
- exemple: le récit de vie clinique aboutit à une transformation de soi-même, de ses relations
- discours militant: parler pour être entendu, pour être diffusé, et changer qlq chose dans le monde
- mémoire des communautés, construire une mémoire collective communautaire afin de renforcer le sentiment d'appartenance à une communauté

**Index**: paradigme de la trace

sans objectif très défini, mais faire en sorte que ca ne se perde pas.
- Un patrimoine à ne pas perdre.

# Questions

### comment être sur que ce qui est témoigné a une source fidèle et n'est pas mélangé avec les émotions.
- dans les situations de guerre, problème du témoignage propagandiste vers la construction d'une mémoire collective

Tout témoignage comporte une part d'émotion, d'erreur, éventuellement de la manipulation.
Faire avec: méthode pour que les personnes progressivement ôte le masque.

- Prendre le temps (> à 45min) pour distinguer le front office (discours déjà pensé, réfléchi, construit) et le back office. Connivence qui s'installe, dire tout ce qu'il y a à dire. Conserver du matériel, des questions pour poursuivre l'entretien.
- Demander des détails, des précisions
- Vérifier les faits, recouper.



### Récit de vie à vocation thérapeutique:  utilisation du récit de vie en SIC ? risque d'appropriation d'une discipline (psycho)
- conséquence inévitable de tout témoignage oral. Savoir le défendre

### Alessandra Sartorio:  Est-ce qu’il faut respecter une progression dans la préparation d’une interviewe, une progression dans l’intensité des questions, du général au particulier? Autre? Quel type de questions il faut absolument poser ou absolument pas poser?
- progression dans l'entretien: aller du général (front office) au plus particulier, de l'anodin au plus personnel, de l'ancien au plus récent (récit de vie)
- progression du témoignaire (rodage)
- distinguer le droit et la politesse ou l'éthique
    - droit: données personnelles critiques (sexualité, faits de justice) - pas d'utilisation sur un délai de 50 ans, à moins d'avoir une autorisation du témoin (contrat, pacte (réciprocité), contrat moral (explicitation de la démarche))
    - attention aux particularités selon les statuts des témoins: mineurs, fonctionnaires, salariés, etc..

### Christina Dumetz : Après les (2 à 3) "prétests", combien de temoignages faut'il pour que le résultat soit significatif pour une sérieuse recherche  (pour sa thèse)?
- en qualitatif: on arrête quand on n'obtient plus de nouveaux résultats, on arrive au bout de la recherche. Tant qu'il y a de la surprise, on peut continuer.

### Julie Feltz Faut-il toujours anonymiser les entretiens ? Je travaille sur des entretiens avec des artistes et dans ce cas il me semble qu'il y aurait un intérêt à ce que leur parole soient identifiées, puisqu'il faudrait ensuite faire le lien avec leur travail artistique...
- anonymisation nécessaire pour des sujets critiques: nuisances possibles pour la vie du témoin.
- anonymiser suppose de changer de nom, mais surtout d'empêcher toute reconnaissance possible du sujet.

### Azin Mohammadali-doctorante HAR/ED 138 J'ai une question sur la traduction des témoins. C'est-à-dire comment possible publier ou traduire le discours sans détourner l'idée ou être fidèle au témoignage ?
- traduire c'est trahir.. suppose des transformations inévitables.
- faut il tout transcrire ? cela dépend si l'on est intéressé par les hésitations, les émotions fines, les éléments de langues, etc. Pas forcément nécessaire.
- les archivistes font une fiche chrono-thématique : {TC-IN, TC-OUT, mots-clés, pourquoi c'est important, etc.}. format de fichier VTT ? IIIF ?
- formats existants (mais peu utilisés car projets anciens): voir le projet Advene et Lignes de temps (IRI): Schéma XML dédiés, puis plus tard ont été développées des passerelles vers du LOD et W3C Fragments

### autres

- récit de soi ou récit d'un fait ?
- question de la confiance ?

Anne-Laure Stérin sur le droit des témoins (co-auteur témoins et témoignaires)

## Liens et Références

- [passerelle de mémoires](https://passerelle.hypotheses.org/author/scopsi)
- https://passerelledememoires.fr/formation
- Guide des archives orales, réalisé dans le cadre du projet CollEX-Persée« Réseaux pluridisciplinaires et pratiques documentaires : les transitions politiques en Amérique latine des années 1960 aux années 2000 ». Il est porté par La contemporaine et l’IHEAL en lien avec le GED Condorcet et l’Institut des sciences sociales du politique et consultable à l'adresse suivante :  https://lcbam.hypotheses.org/category/guide-sur-les-archives-orales


_à venir_
