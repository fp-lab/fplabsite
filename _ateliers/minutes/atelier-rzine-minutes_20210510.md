<!-- ---
title: FPLab - Atelier pratique - Rzine
date: 2021-05-10
tags: fplab, atelier, rzine, ressources, protocole éditorial, collaboration
--- -->

###### FPLab - Atelier pratique - 10 mai 2021

# Rzine, Documenter les pratiques de R en SHS

Avec:

- Hugues Pecout (CNRS, FR CIST) - Responsable
- Timothée Giraud (CNRS, UMS RIATE) - Responsable
- Marion Le Texier (Université Rouen Normandie, UMR IDEES)
- Sebastien Rey-Coyrehourcq (Université Rouen Normandie, UMR IDEES)
- Grégoire Le Campion (CNRS, UMR Passages)
- Ronan Ysebaert (CNRS, UMS RIATE)
- Madeleine Hubert (Bnu, Strasbourg)
- François Quéré (Bnu, Strasbourg)


## Tour de table

- Hugues Pecout : R pour sa Polyvalence et sa reproductibilité
- Marion Le Texier : usages pédagogiques
- Ronan Ysebaert : réfléchir au processus de publication, flux d'information, pour les collections du flux Rzine

Profils  et pratiques différentes, mais envie comunne : produire de la documentation sur R.

Langage fédérateur pour les disciplines SHS.

Collaboratif et interdisciplinaire, en phase avec CIST

statut : "projet exploratoire" pour prendre le temps de construire un projet, sans livrable ou objectif final.
Soutien financier du CIST, dont 30% de temps ingénieur.

Pour que le projet persiste, ca ne doit pas tenir sur un noyau mais sur une communauté.

Point de départ: 2 constats:
- Etat des lieux de la documentation sur la pratique de R, y compris hors des SHS. Résultat : une documentation déjà existante + émergence des pratiques spécifiques.
    - réflexion sur la pertinence d'une nouvelle documentation. pertinence selon les publics, notamment pour les débutants.
- les diff. initiative de documenation étaient très délimitées, autour d'une discipline, d'une institution.

2 conclusions:
- il faut faire l'effort de s'adresser à un public large et hétérogène./
- établir une communauté assez large, au delà d'un groupe d'individu.

2 axes ont émergé:
- dév. d'une plateforme collaborative
- création d'une documentation sur R

### Rzine.fr

considérations techniques et politiques:
- compétences initiales
- faciliter une gestion collaborative du site
- minimiser l'appartenance du site à un individu ou à un petit groupe

orientation vers des outils/infrastructures.

**Rzine est construit avec R** (package Blogdown utilisant le framework Hugo, template Hugo Academic adapté)

- Site statique: uniquement composé de fichiers. Le site peut alors être stocké sur un repertoire git, via gitlab.huma-num.fr

- licence: le dépot rzine est placé sous CC-BY (?)

- Serveur:
- nom de domaine: via CNRS, durée indéterminée

affichage institutionnel minimal

### Contenus
4 grand types de contenus :
- Actualité
- Auteur
- Publication (les ressources sous diff. formats : vidéo, texte...)
- Projet

Actualité:
- séminaires, actions, colloques

Projets:
- organisés par type de projets

Auteurs:
- une quarantaine.
- mise en valeur des contributions: construction de la communauté

Ressources référencées:
- 300 ressources
- filtres: thématiques, types de ressources, sources, date.
    - travail à faire pour améliorer ces filtres

Ces 4 grands types de contenus, ouverts aux contributions externes: permettraient de construire une communauté large.

### Principes du site

- Décentralisation de l'ajout de contenus: 8 personnes formées à l'ajout de contenus sur le site
- Clarification de la ligne éditoriale du site: positionnement par rapport aux ressources produites par le secteur privé
- Mise en place d'un comité éditorial: contrôle du contenu et application de la ligne éditoriale

Travailler à l'élargissement disciplinaire

Comité joue un rôle pour "la collection Rzine"

### Collection Rzine

lancement en 2021

- Public le plus large possible
- libre de droit
- contenus soumis à comité de lecture
- association des contenus à un DOI
- code entièrement reproductible
- publications produits en R : système de relecture basé sur git. Puissance en terme collaboratif et reproductibilité.

Complexe à utiliser, mais potentielle montée en compétence sur ces bonnes pratiques.

2 types de contenus:

- fiche: document didactique avec méthode, fonctionnalité, traitment particulier
- manuel: couvre l'ensemble d'une pratique ou d'une méthode d'analyse
    - ambition de les faire produire par des collectifs interdisciplinaires

on distingue :
- les Documentations externes, référencées dans Rzine. Sélection avec système de vote.
    - pas d'autorisation demandée, sauf pour afficher un auteur.
    - ressources déjà en ligne et accessible
- fiches: format de publication avec critères attendus
    - aglométation de fiches dans des manuels


## Questions

- indexation de ressources automatique ?
- Validation par le comité éditorial
    - référencement externe sera plutôt un comité autour du site et de sa maintenance
    - les fiches feront appel idéalement à un comité dédié

Les fiches peuvent être écrites par tout le monde.

- Référencement de contenus "privés" ? si le contenu est accessible gratuitement, sans pub, alors le contenu pouvait être publié

- Thématiques exclusives ?


- niveau de la ressource: débutant/moyen/expert:
    - pour chacune des ressources, on souhaitait indiquer le niveau, mais trop problématique (on peut être débutant sur un thème et expert sur un autre).

Pas de multi-thématiques dans le template Hugo utilisé

- Pour les fiches : on  précisera plutôt les prérequis, plutôt que d'indiquer le niveau d'expertise.
    - grille d'évaluation connue avant l'écriture d'une fiche: sous l'angle de l'accompagnement.
    - expertise thématique et expertise technique
    - evaluation ouverte sous git/issue

- Evolution dans le temps ?


Evaluation ouverte sous git
- journal of software, etc.
- si les allez-retours entre reviewers et auteurs sont ouverts, on est alors dans l'amélioration du contenu, et non dans de la sanction.
- il ne s'agit de reproduire une documentation, mais contextualiser le processus de soumission avec git: identifier des rôles, certains acteurs peuvent se concentrer sur une partie du process notamment.

Ouvrir avec la possibilité de participer au projet à diff. étapes.

limitation de l'instance gitlab huma-num.


Appréhension de git:
- comment tirer tout le monde vers le haut ? comment établir cette montée en compétence ?
- enjeu de pouvoir s'adapter aux déposants et mettre en place un protocole adapté

formation git prévue pour l'équipe

Visibilité du projet gitlab/github:

- les paquets R sont plutôt sur github
- besoin d'amener la communauté à utiliser github pour s'adresser aux gestionnaires de paquets


Création d'une communauté
- Quelles interactions avec d'autres réseaux
- quid du multilangue/international
    - choix du français
    - les sources multilangues peuvent être référencées, mais les fiches seront françaises

Ambition de réunir les gens, faire des groupes de travail pour des manuels spécialisés : générer des rencontres, des synergies interlabo, etc.

Principe:
    - mettre en lien les diff. auteurs pour produire une fiche commune plutôt qu'une multiplication de fiches similaires


Le projet initial consistait à produire de la documentation, pas de faire un site web.
Construire une communauté : c'est un appel à contribuer, fédérer les initiatives

Dév. d'un package pour faciliter l'ajout de contenu, et la production d'une fiche.
Pas de pf en marque blanche prévue.


Institutionnel:
- ressources ancrées dans les institutions (comme Parcours R): pas de résistance au référencement.

Dispositif d'évaluation de la plateforme elle-même.

Pas d'intégration de commentaire/forum dans le site. trop lourd à gérer.

Dynamique communautaire: Démarche d'ouverture et de visibilisation des auteurs/ressources.

Production de fiches par des débutants, en co-auteur avec des utilisateurs plus experts.

Utilisation des réseaux sociaux: twitter uniquement, sur lequel se trouve déjà la communauté R.
Pas de politique particulière.
