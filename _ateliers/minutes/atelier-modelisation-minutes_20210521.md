<!-- ---
title: FPLab - Atelier avec Gérald Kembellec
subtitle: Modélisation des données. Une maïeutique interdisciplinaire pour des données de qualité
date: 2021-05-21
keywords: atelier théorique, modélisation, données, interdisciplinarité,
--- -->

###### FPLAB Atelier théorique - 21 mai 2021
# Modélisation des données. Une maïeutique interdisciplinaire pour des données de qualité, avec Gérald Kembellec


[_&rarr; page de l'événement_](https://fplab.parisnanterre.fr/ateliers/geraldKembellec_21052021.html)

[_&rarr;présentation_](https://docs.google.com/presentation/d/1-VT8-RJPuGlfqNLqpx3MVXMtNUY4RHvPARJLZEtk3Vo/edit#slide=id.p)

Retour sur le premier projet en HN qui a amené à travailler le sujet de la modélisation de données

### Préambule

injonction au DMP depuis 2019, dès le montage de projet:
- comment on collecte, stocke, partage, restituer, diffuser les données

LE DMP comme un outil pour formaliser ces aspects: formulaire en ligne.

COnsidérer le DMP comme une étape constructive pour son projet :
- nous oblige à modéliser: penser le projet de recherche en amont et réfléchir aux données, leur type, leur volume, leur stockage, une éventuelle anonymisation, choix de licence, etc.
- anticiper la valorisation du projet, et celle des données

### Modéliser et co-construire: spécificité "interdisciplinaire" des HN

transfert des méthodes d'une discipline à une autre, mais avec une finalité inscrite dans sa propre discipline.
travailler à plusieurs, mais y trouver de la matière et de l'intérêt à sa propre discipline

!= transdisciplinarité: travailler avec une vision discontinue entre discipline (co-construction d'objet)

Interdisciplinarité:
- échange de méthodes: suppose un dialogue et un échange de connaissances.

Les HN sont par nature interdisciplinaires.

Objet de recherche: penser un phénomène, observé depuis une ou plusieurs disciplines

démarche d'écoute de l'autre, et démarche de maïeutique

enrichissement scientifique mutuel

### Modéliser son objet de recherche: maïeutique en dialogue

méthode socratique d'échange : interroger les autres pour exprimer leurs connaissances, leurs points de vue.
écouter et reformuler, séries de boucles itératives

questionner, écouter, retranscrire (sous forme schématique), confronter et négocier les points de vue

outil SyMoGIH http://symogih.org/ (labo LARHRA): système pour comprendre les liens (sémantiques) entre acteurs de l'histoire et tous les concepts d'histoire (lieux, actions, objets, faits, etc.)

considérer que cette modélisaiton permet de prendre du recul sur son objet.

mocodo.wingi.net : logiciel permettant de modéliser un objet de recherche facilement sans se préoccuper de la modélisation de la base de données.

Modélisation comme une transcription conceptuelle d'un objet de recherche. Ce n'est pas nécessairement (ou pas encore) une pratique informatique.

### Réaliser un dispositif de médiation après la modélisation

du modèle au dispositif:

FUD, Heurist, OmekaS, NakalaPress,

### Retour sur un projet interdisciplinaire

Histoire de l'art, SIC, Sociologie

Corpus de critiques d'art

approche monographique des critiques

documentation et modélisation des échanges pour comprendre les demandes des acteurs

### Modèle co-construit.

du modèle aux interfaces répondant aux besoins usagers, mais aussi à la réalité des modèles documentaires et informatiques

prosopographie: analyser les acteurs par leurs caractéristiques

### à l'Origine du projet: la maïeutique

1ere modélisation trop simpliste par rapport au besoin des chercheurs en HA.

la maïeutique pour faire émerger un modèle plus complexe, ex: prise en compte de personnes, pseudo, collectif, etc.

### Importance de la qualité des modèles et des données

granularité pour :

- interroger la base de manière pointue
- enrichir les contenus des pages générées:
    - référencements, etc.
    - interrogation sémantique
    - identification individuele des contenus
    - détection et moissonnage des notices
- augmenter la granularité des recherches et la finesse d'interrogation via du raisonnement d'inférence avec un moteur sémantique (de type SPARQL), par ex. pour une meilleure citabilité des auteurs
-     


réconcilier des pratiques (formats) avec des exigences de documentation

- collecte
- relecture
- saisie   contrainte qualitative: la saisie devait accéder à une "autorité sociale énonciative et institutinonelle" (Gérard Leclerc)
- Dépôt

### Interfaces

- visuelles: IHM, dont formulaire de recherche en français vernaculaire
- systématiques: ETL, API

+ microformat/microdonnées: permettent aux moteurs de détecter des connaissances à l'intérieur d'un contenu

voir openLink Structured Data Sniffer: http://osds.openlinksw.com

### Vers la prosopographie

science annexe de l'histoire: observation des personnes dans leur milieu (à la sociabilité identifque)

ex, projet de Lamassé/Cohamé (?): les universitaires au moyen-âge

la modélisation des données en amont a permis de faire ressortir des éléments prosopographiques : salons, sociétés syndicats, oeuvres, critiques, pseudo, formations, distinctions, profession des parents, etc.

utilisation de notices d'autorité (ISNI, wikidata) pour faire du raisonnement par graphe

### visualisations sur le corpus (post-hackathon)

PCLanglais: http://vintagedata.org/divers/reseauCritiquesArt/

## Questions

- Isabelle Marthot Santaniello: prosopographie, avec données fines sur les individus. La source des données est-elle conservée dans la base de données ?

les données étaient déjà traitées (thèse, master), mais aussi ISNI (référentiel d'autorité).

pas de lien aux sources des données


- Laurent (MetaindeX): BD type NoSQL moins de contraintes qu'une modélisation rigide en amont.
> avec le recul, à quel point l'effort poussé de modélisation et de contraintes posées sur l'outil est-il à l'usage rentable pour le chercheur, comparé à un stockage dans une BD peut-être moins contrainte (type NoSQL typiquement) mais disposant d'un langage de requête avancé

À l'usage, certains éléments n'ont pas pu être modélisés et donc  n'ont pas pu être intégrés

Evolution avec le principe du _datalake_ et de son traitement pour des données structurées

voir papier sur BD SCM

- Claude Mussou: dimension quanti est toujours complétée par un retour aux sources et aux documents

- Valérie: Comment intégrez-vous  le suivi de la maintenance de la plateforme, les mises à jours continuent-elles dans cette ressource ou est-ce un projet considéré comme terminé?

BDD figée, phase d'exploitation

Données archivées sur Zenodo

- implémentation du modèles

Utilisation de micro-données (schema.org) plutôt que du RDFA

Palimpseste de descriptions

- Raphaëlle Krummeich (IRIHS)
Quid de FRBRoo (BNF) ou le Cidoc-CRM et ses extensions?

https://ontome.dataforhistory.org/
