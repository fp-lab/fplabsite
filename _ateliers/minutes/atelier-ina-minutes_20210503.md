<!-- ---
title: FPLab - Atelier théorique - INA
date: 2021-05-03
tags: fplab, atelier, ina, video
--- -->

###### FPLab - Atelier théorique - 3 mai 2021

# INA, Faire de la recherche avec les archives audiovisuelles

Intervenants:

- **Claude Mussou** : Introduction
- **Laetitia Larcher** (chargée de mission documentation à l'INA) : Les sources disponibles pour la recherche
- [**Cecile Meadel**](https://www.u-paris2.fr/fr/universite/enseignants-chercheurs/mme-cecile-meadel) et [**David Doukan**](https://www.linkedin.com/in/david-doukhan-588074107/?originalSubdomain=fr) : _Gender Equality Monitor_, égalité et parité Hommes/Femmes dans les médias
- [**Matteo Treleani**](https://fr.wikipedia.org/wiki/Matteo_Treleani) et [**Olivier Buisson**](https://www.linkedin.com/in/olivier-buisson-60a2387/?originalSubdomain=fr) :  _Crossing Borders Archives_ et l'outil Snoop, quelles images symboliques de l’Europe dans les médias audiovisuels&nbsp;?
- [**Jean Carrive**](https://www.linkedin.com/in/jeancarrive/?originalSubdomain=fr) et [**Franck Mazuet**](https://antract.hypotheses.org/29) :  _ANTRACT_, un corpus historique à l'épreuve de la transdisciplinarité.



## Introduction par Claude Mussou

Mobilisation des collarborateurs de l'INA vers de nouvelles approches d'études et d'exploration des corpus de l'INA

Séance sur des projets et des outils innovants, exploratoires.

Video: Archives inscrites sur des médias technologiques qui ont nécessité des instrumentations particulières d'inscription (Bachimont)

Emergence des humanités numériques.
accompagnement de la recherche à partir d'outils innovants, y compris IA, DLearning.

## Laetitia Larcher : périmètre des sources et des fonds disponibles pour la recherche

Fonds Télé, Radio et Web
documentation écrite et collections audiovisuelles d'institutions

19M d'heures de radio et télévision
40K objets web
34K films de cinéma

Pluri-médias
revul historique de 70 ans (télévision), 80 ans (radio)


documentation écrite: monographies, documents déposés, travaux universitaires, périodiques spécialisés, archives écritures, etc.
Remise en perspective des fonds audio-visuels

### Métadonnées annexes et accompagnement

- outils sur les postes de consultation : segmentation de programme, annotation de corpus
- transcription de la parole, reconnaissance automatiques (genre, visages, OCR)

Accompagnement:
- in situ à l'inathèque/BnF
- accompagnement individualisé dans le cadre de projets cadrés
- catalogue du dépot légal en ligne (intégralité des notices, sans les médias)

## _Gender Equality Monitor_, égalité et parité Hommes/Femmes dans les médias

David Doukhan - ingénieur de recherche à l'ina, ML applkiqué à l'audio/vidéo
Cécile Maedel: laboratoire .; sociologie des usages

Projet GEM:
- décrire les différences de représentation et de traitement existant entre les femmes et les homme s dans les médias
    - constat initial: sous-représentation des femmes dans tous les médias
- comprendre ces différences de représentations, affiner la caractérisation des genres
- Aborder des matériaux peu traités


### concilier les approches manuelles avec les approches automatiques.

Gradient Volume/automatiques - Manuel/complexité

mais complexité devient rapidement subjectif (interprétation)

Manuel : couteux en ressources humaines, suppose des échantillonage qui introduisent des biais

Automatique:
- repérage de phénomènes simples
- analyse exhaustives
- indicateur numériques pour alimenter le débat citoyen
- permet d'identifier des problématiques qui seront adressées manuellement;

trouver le bon compromis entre ces deux approches.

### Verrous scientifique et technologiques

- comment qualifier certaines notions : par exemple le manterrupting
quelle signification pour un recouvrement de voix
- transdisciplinarité

### Equipe Carism
contexte sanitaire amène à repenser le projet

constat initial : femmes dans des positions de témoignages individuels et profane. Figure de la temun (?)

3 corpus:
- féminisme
- viols et violence (surnombre de violences intrafamiliales dûes au confinement)
- pandémies

### Analyse transdisciplinaire: les incrustations

- question: qui parle pendant le covid ?
    - femmes majoritaires dans les métiers du soin
- matériau: incrustation/bandeau (nom + qualité)
- concept: l'autorité de pouvoir (institutionnel), de savoir, de notoriété
    - encodage de l'autorité (+ / -) ?

1900 personnes analysées

### Chaîne de traitement

détecter les zones d'incurstation
regroupement
OCR sur les bandeaux (pas toujours efficace)

Constitution de documents partagés (GG drive) préremplis automatiquement nom, prénom, description,

intervention manuelle :
    - correction des contenus textes
    - codage genre, domaine (parmi 9), autorité symbolique (+/-)

Pour chaque incrustation : 20sec d'intervention humaine experte.

Sur 24h de BFM, 130 incrustations : 40min d'expertise humaine.

permet d'analyser des grands volumes grace à une première analyse automatique.

Résultat :
- femmes largement minoritaires dans l'info télé, distinguant journalistes/hors-journalistes.
- hommes ont plus souvent un statut d'autorité que les femmes

### Biais d'échantillonnage

seule l'exhaustivité permet d'éviter certains biais (ex: taux de présence des femmes doublée d'un jour à l'autre sur FR2)

### GMMP

- 1 monitoring mondial des médias sur une journée entière : https://waccglobal.org/our-work/global-media-monitoring-project-gmmp/
- fiche type
- fait par des bénévoles, activistes, mais en france: chercheur·e·s

Pourquoi faire intervenir l'INA
Objectif: faire parler autrement ces données
3 questions additionnelles:
    - question de l'autorité
    - utilisation de la langue inclusive (parler de manière inclusive)
    - est ce que les codeurs considèrent que les personnes appartiennent aux catégories ethno-racisées.


- Décompte automatique du temps de parole des femmes et des hommes (données fournies aux MIP ARtech)
- comparaison à d'autres traitements (identification des visages)
- Études lexicométriques

## Projet Crobora - Matteo Treleani
_Crossing Borders Archives_

- anr-crobora.fr (démarrage en mars 2021)
- circulation des objets audiovisuels dans le paysage médiatique

- actuellement : phase de collecte

Constat préliminaire: présence d'archives AV dans l'espace numérique. constitue notre mémoire collective.
Les images sont répétées et façonnées par les contextes dans lesquels elles apparaissent.
analyse des redondances d'une meme images dans plusieurs environnements.

Système complexe de médiation qui déterminerait la circulation. analyse des logiques d'autorité dans ces circulations: humaines, interfaces, dynamiques culturelles, etc.

3000 sujets télévisés utilisant des images d'archives de la construction européenne

mise en place d'une bdd pour comprendre ces réusages

projet s'appuie des outils de l'INA, dont Snoop.

Olivier Buisson, chercheur à l'INA, développe un moteur de recherche.
- principe de l'outil: remettre l'utilisateur au centre
- interaction avec l'IA afin de créer, enrichir, exploiter à large échelle des bases de connaissances

"vérité terrain" ou corpus

Snoop: modélisation de catégories/concept (ici tigre)


Utilisation de Signatures pour retrouver les redondances/occurrences de certaines images

Dans ces archives, sont présentes des images illustratives.
déjà indexées/documentées par les documentalistes de l'INA.

Autres images de raccord, illustratives, non-indexées.

## Antract - analyse transdisciplinaire des actualités filmées (1945-69)

Les Actualités Françaises: actualités filmées et diffusées dans les cinemas juqu'à l'arrivée de la télévision en 69.

Corpus: 1262 journaux (1 par semaine), 20232 sujets, tapuscrits des commentaires, notices documentaires

thématiques:
- Foules et émotions dans la 2nd moitiée
- prisonniers de la 2nd guerre
- stars du cinema francais
- exposition des foules (?)

documentation très riche: notices, tapuscrit des voix off, description de l'image plan par plan.

- Transcription automatique + reconnaissance entités nommées
- tapuscrit: page de sommaire d'un journal + intégralité des commentaires
    - classifier les deux types d'images
    - OCR des tapuscrits (via google)
    - alignement des sections de pages de commentaires avec les vidéos (croisement avec algorithme d'ASR (transcription voix off))
    - analyse automatique d'images animées: algo de "video captioning" : génération d'1 phrase descriptive à partir d'une image. pas toujours pertinent, difficulté pour prendre en compte le contexte
    - identification des visages (apprentissage progressif et itératif des visages)
    - Okapi: plateforme d'annotation et d'exploitation sémantique (système documentaire permettant à chaque utilisateur de spécifier son propre modèle documentaire). Agencement des lignes de temps et thésaurus paramétrable.
    - textométrie (avec TXM): analyse des transcriptions, liens entre vidéo et textes.

Franck Mazuet (réalisateur documentaire): thèse en cours sur les Actualités françaises.

- quelle grammaire audiovisuelle/cinématographiques pour la réalisation de ces actualités ?
- se mettre d'accord sur une valeur de plan, et une nomenclature des valeurs de plan: question d'interprétation qui peut être délicate: gros plan, très gros plan, plan moyen, etc.
- vers une grammaire cinématographique récurrente ?

au-delà de la grammaire: pourquoi cette grammaire et comment ?
qui a filmé, comment, dans quelles conditions ?
en quoi y a t il une standardisation formelle de ces images.

exploite l'identification des plans et la textométrie

montre les connexions de la mise en scene des actualités avec une volonté politique sociale ou culturelle, dépendant des techniciens autour de la production de ces sujets.


## Questions

Claude Mussou: dimension exploratoire à la fois des pratiques SHS, mais aussi des algorithmes

Outil de transcription automatiques de la parole: outil du laboratoire Lium (https://lium.univ-lemans.fr/lium/lst/), en collaboration avec le LIA.
un des meilleurs outils de l'état de l'art.

Outils de l'INA en Open Source - page github.com/ina-foss

Ina Speech segmenter : détecte un signal audio et distingue les bruits, les voix d'hommes, de femmes.

https://larevuedesmedias.ina.fr/

il n'existe pas de catalogue des sources du DL web

Utilisation du DL Web pour Crobora
