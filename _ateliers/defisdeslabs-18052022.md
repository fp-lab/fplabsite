---
layout: atelier
#what displays when the item is clicked:
type: Rencontre Humanistica 2022
title: >-
  Les défis des labs
subtitle: >-
  Comment accompagner les humanités numériques&nbsp;?
tags: []
image: /assets/img/event/pmclab.jpeg
alt: >-
  PMClab © UMPC
infos:
  date: 2022-05-18
  horaire: 9h30-12h30
  lieu: salle B01-01 - CNRS (54 bvd. Raspail)
  online: no
  etat: coming
---

<style>
  h3 {
    margin: 1.7em 0 1em;
  }
  :target::before {
    content: "";
    display: block;
    height: 130px; /* fixed header height*/
    margin: -130px 0 0; /* negative fixed header height */
  }
</style>

### Humanistica 2022 - Session de travail

<div style="width:40%;float:right; margin-left:1em;">
<iframe src="/assets/pdf/Humanistica2022_proposition_FPLAB-invitation.pdf" width="100%" height="500px">
</iframe>
<p style="float:right; font-size: smaller;">
<a href="/assets/pdf/Humanistica2022_proposition_FPLAB-invitation.pdf">Ouvrir le pdf</a>
</p>
</div>

<p>Le <a href="https://fplab.parisnanterre.fr">FabPart Lab</a> du Labex <a href="https://passes-present.eu">Les Pass&eacute;s dans le pr&eacute;sent</a> (Universit&eacute; Paris Nanterre) organise une rencontre consacr&eacute; aux initiatives d&rsquor;accompagnement &agrave; la recherche en humanit&eacute;s num&eacute;riques. Sous des d&eacute;nominations diff&eacute;rentes (DH Lab, M&eacute;dialab, Data Lab, centre d&rsquor;appui, cellule de soutien, etc.), plusieurs structures et entit&eacute;s ont &eacute;t&eacute; cr&eacute;&eacute;es r&eacute;cemment au sein des universit&eacute;s, des institutions de recherche ou des institutions culturelles. Ces initiatives tentent de r&eacute;pondre aux besoins croissants des chercheur&middot;e&middot;s et des &eacute;quipes de recherche dans les diff&eacute;rentes disciplines en sciences humaines et sociales pour ma&icirc;triser les bonnes pratiques num&eacute;riques en termes de gestion et de valorisation des donn&eacute;es de la recherche et pour s&rsquor;approprier les m&eacute;thodologies num&eacute;riques de traitement, d&rsquor;analyse et d&rsquor;&eacute;ditorialisation de ces donn&eacute;es.</p>
  <p>Or, les communaut&eacute;s en humanit&eacute;s num&eacute;riques se sont engag&eacute;es tr&egrave;s t&ocirc;t sur les probl&eacute;matiques de litt&eacute;ratie num&eacute;rique et d&rsquor;ouverture de la recherche, aujourd&rsquor;hui largement prises en compte au niveau institutionnel. De ce point de vue, ces entit&eacute;s d&rsquor;accompagnement &agrave; la recherche &eacute;largissent ces pr&eacute;occupations initialement exprim&eacute;es par la communaut&eacute; des humanit&eacute;s num&eacute;riques &agrave; d&rsquor;autres domaines des sciences humaines et sociales. Il nous semble alors pertinent d&rsquor;initier un dialogue entre ces initiatives, sp&eacute;cifiquement sous l&rsquor;&eacute;gide des humanit&eacute;s num&eacute;riques, pour b&eacute;n&eacute;ficier de l&rsquor;exp&eacute;rience de la communaut&eacute; en la mati&egrave;re.</p>
  <p>Pour cette session de travail, nous invitons plusieurs structures de type DH Lab, Data Lab ou toute cellule de soutien au num&eacute;rique d&eacute;di&eacute;e &agrave; la recherche en SHS, afin d&rsquor;ouvrir un espace d&rsquor;&eacute;change sur leur exp&eacute;rience, leurs difficult&eacute;s et leurs besoins. La session de travail sera l&rsquor;occasion pour chaque entit&eacute; de pr&eacute;senter sa philosophie, ses ressources et son public (type de projets, type de besoins), mais aussi de pointer les difficult&eacute;s rencontr&eacute;es. L&rsquor;un des objectifs de la session sera d&rsquor;identifier les obstacles r&eacute;currents et de d&eacute;gager des pistes communes d&rsquor;am&eacute;lioration.</p>


### Déroulé

- **9h30:** Accueil des participant·e·s
- **10h00:** Ouverture de la rencontre et tour de table
- **11h10:** Le rôle des labs dans les projets de recherche : structuration de l'accompagnement et gestion de la temporalité de la recherche, divergences éventuelles entre offre proposée et demande des chercheurs
- **11h40:** Le positionnement des labs dans l'institution (universitaire ou patrimoniale) et à l’extérieur (relations avec les infrastructures de recherche, mutualisation avec d’autres labs, etc.)
- **12h10:** Perspectives
- **12h30:** Fin de la rencontre

### Participants

- **Marie Carlin** (BNF Data Lab),
- **Brian Chauvel** (PUD Paris Nanterre),
- **Madeleine Hubert** (DataLab de la Bibliothèque Universitaire de Strasbourg),
- **Hélène Jouguet** (IR* Huma-Num)
- **Valérie Julliard** (CERES - Sorbonne Université)
- **Gaël Lejeune** (CERES - Sorbonne Université)
- **Daphné Matelier** (MSH Mondes - Paris Nanterre)
- **Claude Mussou** (INA thèque - LabMedia)
- **Federico Nurra** (Lab du service numérique de la recherche - INHA)
- **Benjamin Ooghe-Tabanou** (SciencesPo Medialab)
- **Pierre Pistre** (CHN - U. Paris Cité)
- **Elsa Tadier** (CHN - U. Paris Cité)

**Organisation :** Nicolas Sauret et Marta Severo (FabPart Lab, Université Paris Nanterre)

---

_Cette session de travail est organisée par le FabPart Lab dans le cadre du colloque [Humanistica 2022](https://humanistica2022.sciencesconf.org/). La séance sera enregistrée et rediffusée lors du colloque._

_Le FabPart Lab est soutenu par le labex Les passés dans le présent et a donc bénéficié de l’aide de l’État géré par l’ANR au titre du programme Investissements d’avenir portant la référence ANR-11-LABX-0026-01._

![logoLabex](https://i.imgur.com/Ns4NJVb.png){: style="width:100px;"} &nbsp;&nbsp;&nbsp; ![logoFPL](https://i.imgur.com/ZGvzoEe.png){: style="width:100px;"}

[mailfplab]: mailto:fplab@liste.parisnanterre.fr
