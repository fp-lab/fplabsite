---
layout: atelier
#what displays when the item is clicked:
type: Atelier pratique
title: Sarah Garçin
subtitle: Outils et méthodes pour la documentation d'un terrain de recherche
tags: [terrain, outils, documentation, collaboration]
image: /assets/img/event/garcin.png
alt: Sarah Garçin
infos:
  date: 2021-03-15
  horaire: 14h-15h30
  lieu: en ligne
  etat: archive
nakala:
  doi: 10.34847/nkl.68abhr0f
  video: 10.34847/nkl.68abhr0f/5f866632e79bbd8d1469fa8824ee0c2c2a47ddfc
notes: atelier-documentation-minutes_20210315.md
---


#### Présentation

La séance nous a permis d'accueillir Sarah Garçin, designer, artiste et enseignante, et co-fondatrice du collectif [l'Atelier des chercheurs](https://latelier-des-chercheurs.fr/) en 2013 avec Pauline Gourlet et Louis Eveillard.

L'Atelier des chercheurs développe [des outils et des méthodes](https://latelier-des-chercheurs.fr/outils) pour la documentation collaborative d'activités pratiques. Sarah Garçin a présenté les principes et la philosophie de ces outils et comment ces outils peuvent être utilisés sur le terrain par les chercheur·e·s pour documenter leurs travaux.
