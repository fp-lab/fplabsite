---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Beatriz Tadeo Fuica
subtitle: >-
  Comment profiter d’une IA pour faire des études cinématographiques ? Réflexions sur une expérience pratique
tags: [traitement automatique, collections audiovisuelles, INA, archives, Snoop, Machine Learning]
image: /assets/img/event/ia-winstead.jpg
alt: Image de Tara Winstead provenant de Pexels
infos:
  date: 2021-12-09
  horaire: 9h30-11h
  lieu:
  online: true
  etat: archive
nakala:
  doi: 10.34847/nkl.40ec7ic4
  video: 10.34847/nkl.40ec7ic4/7f0fab70b39fd6da9c91502797f0801bb7663545
---

#### Présentation
L'INA, partenaire du FabPart Lab, mène sur ses archives une politique de recherche associant l'accueil de chercheur·e·s et le développement d'outils innovants d'exploration et d'analyse de ses archives. Pour cet atelier, Beatriz Tadeo Fuica revient sur sa recherche dans les archives de l'INA et sur la manière dont les outils de la recherche viennent reproblématiser cette dernière.

> L’objectif de cet atelier est de partager une expérience de recherche sur des films historiques ayant circulé entre la France et trois pays de l’Amérique latine (Argentine, Brésil, Uruguay) pendant les années 1950. Ayant été diffusés à la télévision française, ces films font partie de la collection de l’INA. Pour analyser les images qui ont circulé grâce à ces films, nous avons utilisé Snoop, une intelligence artificielle développée par l’INA et l’INRIA, qui nous a aidé à identifier dans ce corpus des images montrant des objets similaires.
>
> En choisissant une approche exploratoire, cette expérience nous a permis d’initier une réflexion théorique et méthodologique sur des nouvelles lignes de recherche que des outils de ce type permettent d’aborder.

Beatriz Tadeo Fuica a obtenu son doctorat à l’Université de Saint Andrews au Royaume-Uni. Puis, elle a travaillé comme chargée de recherche Marie Sklodowska-Curie (UE) à l’IRCAV, Université Sorbonne Nouvelle – Paris 3 où elle est actuellement chercheuse associée. Ses travaux sont aussi rattachés au laboratoire Passages XX-XXI de l’Université Lumière–Lyon 2. Ses recherches ont été publiées dans plusieurs revues scientifiques telles que /The Moving Image/, /Quarterly Review of Film and Video/, /New Cinemas/, /Cine Documental/, et /Les Cahiers ALHIM/. Elle a codirigé et contribué à la collection /CEMA: archivo, video y restauración democrática/ et elle est l’auteure de l’ouvrage /Uruguayan Cinema 1960–2010: Text, Materiality, Archive/. Elle est aussi évaluatrice de projets de recherche pour la Commission européenne.
