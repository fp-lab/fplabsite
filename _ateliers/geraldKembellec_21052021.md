---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Gérald Kembellec
subtitle: Modélisation des données. Une maïeutique interdisciplinaire pour des données de qualité
tags: [modélisation, corpus, interdisciplinarité, structuration, données]
image: /assets/img/event/kembellec.png
alt: Gérald Kembellec
infos:
  date: 2021-05-21
  horaire: 10h30-12h
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.dd9fz2se
  video: 10.34847/nkl.dd9fz2se/f72951a759b8cb486a764e6e9c79240423b8a169
notes: atelier-modelisation-minutes_20210521.md
---

#### Présentation
Dès la phase initiale des projets de recherche, les organismes de financement imposent une réflexion sur la collecte, la modélisation, production, le stockage et la valorisation des données de la recherche. Gérald Kembellec propose de voir cette contrainte comme l’opportunité d'anticiper les questions liées aux données des projets de recherche, en effet leur qualité et leur structuration permettront une exploitation et une ré-exploitation maximales.

Il illustre son propos au travers d'exemples pratiques de projets en cours, en Histoire ou Histoire de l'art. Cet atelier est l’occasion de discuter la posture maïeutique interdisciplinaire, point de départ d'une modélisation robuste &ndash; elle-même garante de la réalisation d'un corpus de qualité. Il termine par la question de l'exposition, de l'ouverture et la diffusion des données de la recherche&nbsp;: les enjeux et méthodes.

Gérald Kembellec est MCF en sciences de l'information et de la communication au CNAM (Paris) et actuellement chargé de recherche au département des Humanités Numériques de l'Institut Historique Allemand (IHA, Paris). Ses thématiques de prédilection sont l’accès aux données liées au sein des dispositifs info-communicationnels et les systèmes de recommandations. Il aime exercer ces thématiques sur le terrain des humanités.
