---
layout: atelier
#what displays when the item is clicked:
type: Atelier théorique
title: Julien Schuh et Cyril Bruneau
subtitle: Machine et deep learning appliqués aux SHS
tags: [traitement automatique, machine learning, deep learning]
image: https://robankhood.com/wp-content/uploads/2019/07/deep-learning-768x507.png
alt: Réseau de neurones
infos:
  date: 2021-03-29
  horaire: 14h-16h
  lieu: online
  etat: archive
nakala:
  doi: 10.34847/nkl.cfc0cmys
  video: 10.34847/nkl.cfc0cmys/ee2886d0ec8a5530b8e3e9d1d1fbe365bc3b24f8
notes: atelier-modoap-minutes_20210329.md
---
#### Présentation
Julien Schuh et Cyril Bruneau ont animé un atelier théorique sur les méthodologies et les outils de _machine_ et _deep learning_ dans le cadre de recherches sur les archives et les contenus patrimoniaux.
La présentation s'est appuyée sur les cas d'étude du projet [ModOAP](https://modoap.huma-num.fr/) pour illustrer les principes généraux de ces méthodologies.
