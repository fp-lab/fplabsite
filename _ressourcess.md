---
layout: page
background:
title: Ressources
date: 2020-12-10
author:
categories:
tags:
lead_text: C'est un lead texte !
---

Le FabPart Lab rassemble progressivement une base de connaissances d'outils et de méthodologies numériques en SHS. Les ressources sont distribuées selon cinq phases génériques constitutivess d'un projet de recherche mobilisant un corpus.

<div class="container" id="actions">
<div class="row text-center">
  <div class="col">
    <a href="#conception" class="js-scroll-trigger">
    <span class="fa-stack fa-4x">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fas fa-lightbulb fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <h4 class="service-heading"><a href="#conception" class="js-scroll-trigger">Concevoir le projet</a></h4>
  </div>
  <div class="col">
    <a href="#collecte" class="js-scroll-trigger">
    <span class="fa-stack fa-4x">
    <i class="fas fa-circle fa-stack-2x text-primary"></i>
    <i class="fas fa-hands fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <h4 class="service-heading"><a href="#collecte" class="js-scroll-trigger">Collecter des données</a></h4>
  </div>
  <div class="col">
    <a href="#modelisation-et-documentation" class="js-scroll-trigger">
    <span class="fa-stack fa-4x">
    <i class="fas fa-circle fa-stack-2x text-primary"></i>
    <i class="fas fa-dice-d20 fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <h4 class="service-heading"><a href="#modelisation-et-documentation" class="js-scroll-trigger">Modéliser et documenter</a></h4>
  </div>
  <div class="col">
    <a href="#annotation-et-traitement" class="js-scroll-trigger">
    <span class="fa-stack fa-4x">
    <i class="fas fa-circle fa-stack-2x text-primary"></i>
    <i class="fas fa-tag fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <h4 class="service-heading"><a href="#annotation-et-traitement" class="js-scroll-trigger">Annoter et traiter</a></h4>
  </div>
  <div class="col">
    <a href="#valorisation-et-diffusion" class="js-scroll-trigger">
    <span class="fa-stack fa-4x">
    <i class="fas fa-circle fa-stack-2x text-primary"></i>
    <i class="fas fa-draw-polygon fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <h4 class="service-heading"><a href="#valorisation-et-diffusion" class="js-scroll-trigger">Valoriser et diffuser</a></h4>
  </div>
</div>
</div>


<div class="alert alert-secondary" role="alert" markdown=1>

Vous souhaitez ajouter ou corriger un élément ? La base de ressources est contributive, choisissez votre méthode :

- envoyez nous [un email](mailto:fplab@liste.parisnanterre.fr), nous traiterons votre demande sous peu.
- [déposer une "issue"](https://gitlab.huma-num.fr/fp-lab/fplabsite/-/issues) sur notre dépôt
- ajoutez directement un élément [en suivant les instructions](https://gitlab.huma-num.fr/fp-lab/fplabsite/-/tree/master/_ressources#ajouter-une-nouvelle-ressource)

</div>


<!--
Le FabPart Lab rassemble progressivement une base de connaissances d'outils et de méthodologies numériques en SHS. Les ressources sont distribuées selon les phases d'un projet de recherche, [telles qu'adoptées par la TGIR Huma-Num](https://www.huma-num.fr/les-services-par-etapes/).

![phases recherche](/assets/img/hn-phases-recherche.png)
-->

---

## <span class="fa-stack fa"><i class="fas fa-lightbulb fa-stack-1x"></i></span>Conception
{: .ressourcecategorie}

{% include ressource.html categorie="concevoir" %}

## <span class="fa-stack fa"><i class="fas fa-hands fa-stack-1x"></i></span>Collecte  
{: .ressourcecategorie}

{% include ressource.html categorie="collecter" %}

## <span class="fa-stack fa"><i class="fas fa-dice-d20 fa-stack-1x"></i></span>Modélisation et documentation
{: .ressourcecategorie #modelisation-et-documentation}

{% include ressource.html categorie="documenter" %}

## <span class="fa-stack fa"><i class="fas fa-tag fa-stack-1x"></i></span>Annotation et traitement
{: .ressourcecategorie}

{% include ressource.html categorie="traiter" %}

## <span class="fa-stack fa"><i class="fas fa-draw-polygon fa-stack-1x"></i></span>Valorisation et diffusion
{: .ressourcecategorie}

{% include ressource.html categorie="valoriser" %}
