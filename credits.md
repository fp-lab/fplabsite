---
layout: page
background:
title: Crédits
date: 2021-01-18
author:
categories:
tags:
---

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/) Les contenus du site sont sous licence CC-BY-SA 4.0.
[-- sources](https://gitlab.huma-num.fr/fp-lab/fplabsite/)
{: #license}

Le FabPart Lab remercie :

- Séverine Millot-Dumazert pour les visuels originaux.
- [Sylvia Fredriksson](https://www.flickr.com/photos/sylviafredriksson/) pour la photographie sous licence CC-BY-2.0 utilisée sur [la page d'accueil](/).



Site réalisé avec Jekyll sur la base du thème [Agency](https://github.com/raviriley/agency-jekyll-theme) (MIT licence).  
