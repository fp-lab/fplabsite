---
layout: page
title: Collaboration
subtitle: Des sessions de co-création/conception pour votre projet
---

Cet accompagnement s'adresse aux porteurs de projet voulant initier une dynamique collaborative au sein de leur équipe dans le cadre d'un projet Labex ou dans une phase de conception de projet.
Le FabPart Lab organise à la demande des sessions collaboratives d’une demi-journée ou d’une journée pour faciliter ces dynamiques participatives et contributives, en appliquant des méthodologies adaptées au type de problématique et à la phase du projet.

À titre d'exemple, cet accompagnement peut être organisé pour la conception d'un protocole de recherche, pour la construction d'un corpus, pour l'élaboration d'une méthodologie partagée, ou encore pour mettre en place une annotation collaborative de données.
Il peut s'agir de réfléchir de manière collective à la structure d’une base de données ou d’une archive, d'échanger sur les problématiques scientifiques au cœur du projet dans un cadre interdisciplinaire, ou encore de définir à travers un processus de co-création la forme et/ou la structure de valorisation du matériel récolté dans le cadre d’un projet.

Si vous êtes intéressé par cette action, passez nous voir le lundi pour en discuter lors de [la permanence](/actions/decouverte.html), ou écrivez-nous à [fplab@liste.parisnanterre.fr](mailto:fplab@liste.parisnanterre.fr).
