---
layout: page
title: Accompagnement des projets
subtitle: Un accompagnement scientifique ciblé sur les projets du labex _Les passés dans le présent_
---

Le FabPart Lab est en contact continu avec les porteurs des projets du Labex _Les passés dans le présent_ qui envisagent un volet de médiation, valorisation ou participation numérique.
À partir d’un premier entretien exploratoire, l’équipe du FabPart Lab identifie les besoins du projet et les actions dans lesquels elle pourrait assurer un accompagnement scientifique.

L’accompagnement se concrétise dans un dialogue continu avec les équipes de recherche à propos de leurs choix théoriques et méthodologiques en terme de valorisation et de participation numérique : aide dans le choix des outils, mise en place de la méthodologie, mise en relation avec les services Huma-num ou d’autres services tiers, etc.

En cohérence avec les objectifs scientifiques du FabPart Lab, l’équipe peut intervenir de manière opérationnelle pour les projets adoptant une politique contributive et participative.

L'objectif final de cet accompagnement scientifique est d’enrichir la réflexion globale des partenaires du labex sur les aspects de médiation, valorisation et participation numérique.
