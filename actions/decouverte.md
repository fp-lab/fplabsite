---
layout: page
title: Découverte
subtitle: Une permanence tous les jeudi de 11h à 12h
---

Vous voulez en savoir plus sur les actions du FabPart Lab ?  
Vous avez une idée de projet de valorisation, médiation ou participation numérique que vous voulez discuter avec nous ?  
Vous voulez un conseil à propos de votre projet Labex en cours ?

Le FabPart Lab propose chaque jeudi une permanence de 11h à 12h. La permanence se tient :
- les jeudi d'ateliers : dans la salle de l'atelier qui précède ([→ voir le calendrier des ateliers](/evenements.html)),
- les autres jeudi : au bureau M106 (bâtiment _Éphémère 1_).

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.

La permanence s'adresse à tout·e chercheur·se, post-doctorant·e, doctorant·e, ingénieur·e et personnel des partenaires membres ou associés au Labex _Les passés dans le présent_.
