---
layout: page
title: Sprint
subtitle: Un mois d’accompagnement pour mettre votre projet sur les rails
---

Le FabPart Lab offre un accompagnement intensif pour des projets en cours ou en phase de conception. Cet accompagnement ne vise pas à mettre en place un outil ou à réaliser des tâches spécifiques d’un projet, mais il vise à fournir aux porteurs de projet les soutiens scientifique et méthodologique utiles pour adopter les bons choix en termes de médiation et valorisation numérique. Il vise également à favoriser l'adoption de solutions innovantes et en phase avec la culture de l'ouverture.

Notre équipe pourrait vous aider dans l’identification des outils et des techniques les plus adaptés à votre projet, dans l’estimation des coûts et des temps nécessaires à sa réalisation, et, selon la faisabilité dans le temps de l’accompagnement, dans la prise en main des outils et du workflow de votre recherche ou dans la réalisation d'un prototype.

Cet accompagnement se déroule sur quatre semaines (max 4 heures de temps ingénieur par semaine).

<!-- <div class="alert-primary" markdown=1>
Déroulement type d'un sprint :

- séance 1 :  
- séance 2 :
- séance 3 :
- séance 4 :

</div> -->


Pour accéder à cet accompagnement, vous pouvez faire une demande en remplissant [le formulaire dédié](sprint-demande.html).

Les propositions peuvent être soumises à tout moment et elles seront évaluées au fil de l’eau. Nous vous donnerons un retour sous quinze jours, avec un calendrier de suivi dans le cas où le dispositif du sprint apparait adapté à votre projet.

Si vous avez des questions sur le formulaire, n’hésitez pas à passer nous voir le lundi à [l’atelier découverte](/actions/decouverte.html) ou à nous envoyer [un email](mailto:{{ site.data.sitetext.urls.mailcontact }}).

<a class="btn btn-primary btn-l" href="sprint-demande.html">Accéder au formulaire</a>
