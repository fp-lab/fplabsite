---
layout: page
#what displays when the item is clicked:
title: IRaMuTeQ
type: outil
description: fouille de texte et analyse de discours
ressources:
  - titre: Sur le site d'IRaMuTeQ (documentation, prise en main)
    url: http://www.iramuteq.org/
  - titre: Comment préparer l’analyse de textes de sites Web grâce à la lexicométrie et au logiciel Iramuteq ? par Daniel Pélissier
    url: https://presnumorg.hypotheses.org/187
  - titre: Trois tutoriels vidéos (via Université de Mons)
    url: https://www.youtube.com/watch?v=kJQIeN5LKQw&list=PLxXEiNYFnzZYsrMxth4bHbocu4jYZ3vBT
couts: libre accès
niveau:
prerequis: linguistique, statistique multidimensionnelle
tags:
categories:
  - traiter

---

IRaMuTeQ est un outil de fouille de textes rapide et quasi-automatique. Il nécessite de structurer les données disponibles en amont et de comprendre les méthodologies sous-jacentes issues de la linguistique (comme la lemmatisation) ou de la statistique multidimensionnelle (comme la classification automatique).

L'outil est notamment pertinent pour mener des analyses statistiques sur des corpus texte et sur des tableaux individus/caractères
