---
layout: page
#what displays when the item is clicked:
title: AtoM
type: outil
description: application web de description et de publication d'archives
ressources:
  - titre: Le site officiel présente un grand nombre de ressources
    url: https://www.accesstomemory.org/fr/
  - titre: Webinaire d'introduction à AtoM
    url: https://www.youtube.com/watch?v=kkPYSuSK77k
couts: libre et open source
niveau:
prerequis: administration de plateforme web
tags:
categories:
  - documenter
  - valoriser

---

AtoM, ou "Access to Memory", est une application web et open source pour décrire et mettre en ligne des archives dans un environnement multilingue et multi-dépôts. Il s'adresse aux projets et aux institutions souhaitant exposer des archives ou des corpus patrimoniaux conservés.

AtoM se base sur les standards internationaux de description du [Conseil International des Archives (ICA)](https://www.ica.org/fr) :
  - General International Standard Archival Description (ISAD)
  - International Standard Archival Authority Records (Corporate bodies, persons, and families) (ISAAR-CPF)
  - International Standard for Describing Institutions with Archival Holdings (ISDIAH)
  - International Standard for Describing Functions (ISDF)

Bien qu'AtoM ait été conçu à l'origine autour des normes descriptives internationales de l'ICA (ISAD, ISAAR, ISDIAH et ISDF), il se veut suffisamment flexible pour s'adapter à d'autres pratiques basées sur d'autres normes descriptives (nationales ou locales), notamment celles destinées aux bibliothèques, aux musées et aux galeries d'art.

Actuellement AtoM gère les formats d'import/export suivants: EAD, EAC-CPF, CSV et SKOS.
