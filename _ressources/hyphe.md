---
layout: page
#what displays when the item is clicked:
title: Hyphe
type: outil
description: >-
  crawler web spécialisé pour les SHS, visant à créer, nettoyer et catégoriser un corpus d'acteurs web et leur réseau de liens hypertextes
ressources:
  - titre: Présentation de l'outil sur le site du Medialab
    url: https://medialab.sciencespo.fr/outils/hyphe/
  - titre: Tutoriel sur le wiki de l'outil
    url: https://github.com/medialab/hyphe/wiki
couts: plateforme open-source
niveau: moyen
prerequis: Hyphe est une plateforme davantage qu'un outil. Il requiert une installation locale ou sur serveur (via Docker)
tags: corpus, web corpus, crawler, network visualisation,
categories: collecter

---

Développé par le MediaLab de SciencePo, Hyphe est un logiciel libre de _crawler web_ permettant aux chercheurs de créer des corpus constitués de pages web et de liens entre elles sur un sujet spécifique (par exemple, l'huile de palme ou le coronavirus).

Ces pages web sont sélectionnées par les chercheurs et peuvent être regroupées en « webentités », qui peuvent être des pages individuelles, des sites web, des sous-domaines ou même une combinaison de ceux-ci. Ces entités représentent différents acteurs de la problématique à traiter (par exemple, une personne, une organisation, etc.).
En les crawlant, Hyphe permet de construire itérativement et de visualiser un réseau des relations entre ces acteurs à travers les hyperliens reliant ces webentités.

De nouvelles webentités sont automatiquement suggérées en explorant les hyperliens de chaque entité présente dans le corpus. Les chercheurs peuvent ensuite les examiner dans un processus itératif et qualitatif.
Hyphe permet aux chercheurs de choisir et de catégoriser manuellement les acteurs qu'ils souhaitent ajouter à leur corpus. À ce titre, il doit être considéré comme un outil quali-quantitatif.
