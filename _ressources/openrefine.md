---
layout: page
#what displays when the item is clicked:
title: OpenRefine
type: outil
description: nettoyage et préparation de données
ressources:
  - titre: Site officiel avec documentation et tutoriels en video (anglais)
    url: https://openrefine.org/
  - titre: Ouvrage collectif sur wikibooks référençant plusieurs ressources
    url: https://fr.wikibooks.org/wiki/OpenRefine
  - titre: Présentation succinte (pdf) par Chloé Martin, Jean-Baptiste Pressac (RBDD - CNRS)
    url: http://rbdd.cnrs.fr/IMG/pdf/openrefinecours.pdf
  - titre: Présentation (slideshare) complète des principes, prise en main et pour aller plus loin, par Mathieu Saby
    url: https://www.slideshare.net/27point7/nettoyer-et-prparer-des-donnes-avec-openrefine
couts: libre et gratuit
niveau:
prerequis:
tags:
categories:
  - collecter
  - documenter
  - traiter

---

OpenRefine est un outil puissant pour travailler ses données brutes : il permet de les nettoyer, de les classer, de les transformer d'un format à un autre et de les enrichir avec des services web et des données externes.

Si l'interface fonctionne dans le navigateur (Chrome/Chromium), l'application et les données sont bien localisées sur votre machine.

OpenRefine s'avère vite un outil indispensable pour exploiter un jeu de données existant et en produire un nouveau, nettoyé, restructuré et enrichi, en vue d'un traitement ou d'un export spécifique.
