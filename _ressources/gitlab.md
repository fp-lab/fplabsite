---
layout: page
#what displays when the item is clicked:
title: Gitlab
type: outil
description: plateforme de forge logicielle basée sur le protocole git
ressources:
  - titre: Tutoriel et prise en main de Gitlab (via Framasoft pour son instance Framagit)
    url: https://docs.framasoft.org/fr/gitlab/index.html
  - titre: Introduction au protocole git (via Pierre-Antoine Champin)
    url: https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/intro-git/
couts: L'instance Gitlab de Huma-num est entièrement gratuite.
niveau: facile
prerequis: principes du _versionning_
tags: versionning, collaboration, wiki,
categories: concevoir

---

Gitlab est une plateforme open source de forge logicielle basée sur le protocole [git](https://fr.wikipedia.org/wiki/Git) et intégrant plusieurs fonctionnalités collaborative comme un wiki, un suivi de tickets, l'intégration et la livraison continue. Principalement utilisée pour le développement logiciel, Gitlab est tout à fait adaptée à la maintenance et à l'édition collaborative de contenus et de données structurées.

La TGIR Huma-Num fournit une instance Gitlab ([gitlab.huma-num.fr](https://gitlab.huma-num.fr/)) via son portail [HumanID](https://humanid.huma-num.fr/), constituant une première base de ressources logicielles pour les SHS.
