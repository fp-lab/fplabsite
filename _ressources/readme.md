# Contribuez à la base de connaissances du FPLAB

Vous pouvez contribuer à la base de ressources du FPLAB en ajoutant une nouvelle ressource ou en modifiant une existante.

## Ajouter une nouvelle ressource

1. créez un nouveau fichier depuis gitlab et copiez y la structure ci-dessous :

```yaml
---
layout: page
title: Titre de la ressource
type: outil, méthode, tutoriel, atelier, etc.
description: description succinte de la ressource
ressources:
  - titre: rapide description d'une ressource
    url: lien vers la ressource
  - titre: autre ressource
    url:
couts:
niveau:
prerequis:
tags:
categories: une ou plusieurs parmis [concevoir, collecter, documenter, traiter, valoriser]

---

Un texte explicatif sur la ressource que vous partagez.

```

2. validez la nouvelle entrée et proposer l'ajout en _pull request_

## Modifier une ressource existante

1. Gitlab permet d'éditer un fichier, choisissez "edit"
2. validez la modification et proposez-la en _pull request_
