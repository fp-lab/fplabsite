---
layout: page
#what displays when the item is clicked:
title: Linked Data Patterns
type: livre
description: Un catalogue de modèles/motifs pour modéliser, publier et consommer des données liées
ressources:
  - titre: Le livre dans sa version HTML
    url: https://patterns.dataincubator.org/book/
couts: ouvrage en accès libre
niveau:
prerequis:
tags: modélisation, linked data, données liées, documentation, annotation, métadonnées
categories: documenter, valoriser

---

Compilé par [Leigh Dodds]() et [Ian Davis](), ce livre est une ressource complète sur la modélisation des données liées en contexte scientifique ou patrimoniale.

Au sommaire :

1. **Introduction**
2. **Identifier Patterns**: hierarchical URIs, literal keys, natural keys, patterned URIs, proxy URIs, rebased URI, shared Keys, URL slug,
3. **Modelling Patterns**: custom datatype, index resources, label everything, link not label, multi-lingual literal, n-ary relation, ordered list, ordering relation, preferred label, qualified relation, reified statement, repeated property, topic relation, typed literal
4. **Publishing Patterns**: annotation, autodiscovery, dataset autodiscovery, document type, edit trail, embedded metadata, equivalence links, link base, materialize inferences, primary topic autodiscovery, progressive enrichment, unpublish
5. **Data Management Patterns**: graph annotation, graph per aspect, graph per resource, graph per source, named graph, union graph
6. **Application Patterns**: assertion query, blackboard, bounded description, composite descriptions, follow your nose, missing isn't broken, named query, parallel loading, parallel retrieval, parameterised query, resource caching, schema annotation, smushing, transformation query, URI resolver
