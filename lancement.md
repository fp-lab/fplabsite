---
layout: atelier
#what displays when the item is clicked:
type: Événement
title: Lancement du FabPart Lab
subtitle:
tags: [permanence, participation, découverte, numérique]
image: /assets/img/event/weber-corridor.jpeg
alt: Batiment Weber
infos:
  date: 2021-02-08
  horaire: 11h30-14h
  lieu: online
---


Le FabPart Lab se lance ! [Rendez-vous en ligne]({{ site.data.sitetext.urls.videoconf }}) pour une présentation de ses activités.

**Au programme :**
- Introduction par Ghislaine Glasson Deschaumes et Emmanuel Grimaud
- Quelques mots sur la philosophie du projet par Claude Mussou et  Marta Severo
- Présentation des modalités d'action et d'accompagnement du FabPart Lab
- Echange avec les participants sur les besoins en termes de contribution et de participation

La rencontre sera suivie de la première [permanence](actions/decouverte.html) du FabPart Lab. Elle sera assurée chaque lundi de 12h à 14h dans le bureau M106 (bâtiment _Éphémère 1_) ou [en ligne]({{ site.data.sitetext.urls.videoconf }}).

Rejoignez-nous chaque semaine pour échanger avec nous sur vos projets présents ou futurs, sur vos idées de [sprint](/actions/sprint.html), sur vos choix techniques et méthodologiques ou encore sur vos besoins en termes d'[accompagnement scientifique](/actions/accompagnement.html) dans le cadre de votre projet Labex _Les passés dans le présent_.

Vous pouvez également demander un rendez-vous individuel (en présentiel ou en distanciel) pour discuter d'un projet, obtenir des conseils, ou évoquer [un accompagnement sprint](/actions/sprint.html). Écrivez-nous à [{{ site.data.sitetext.urls.mailcontact }}](mailto:{{ site.data.sitetext.urls.mailcontact }}).

Plus d'information sur [la permanence](/actions/decouverte.html).
